---
title: 'Timeline: Development of OER in Germany'
published: true
date: '07-07-2019 08:16'
publish_date: '07-07-2019 08:39'
external_links:
    target: _blank
    mode: active
fontawesome: use_global
animate_css: use_global
wow_js: use_global
google_prettify: use_global
foundation_abide_js: use_global
foundation_accordion_js: use_global
foundation_accordionMenu_js: use_global
foundation_drilldown_js: use_global
foundation_dropdown_js: use_global
foundation_dropdownMenu_js: use_global
foundation_equalizer_js: use_global
foundation_interchange_js: use_global
foundation_magellan_js: use_global
foundation_offcanvas_js: use_global
foundation_orbit_js: use_global
foundation_responsiveMenu_js: use_global
foundation_responsiveToggle_js: use_global
foundation_reveal_js: use_global
foundation_slider_js: use_global
foundation_sticky_js: use_global
foundation_tabs_js: use_global
foundation_toggler_js: use_global
foundation_tooltip_js: use_global
foundation_util_box_js: use_global
foundation_util_keyboard_js: use_global
foundation_util_mediaQuery_js: use_global
foundation_util_motion_js: use_global
foundation_util_nest_js: use_global
foundation_util_timerAndImageLoader_js: use_global
foundation_util_touch_js: use_global
foundation_util_triggers_js: use_global
---

<iframe src="https://blog.flavoursofopen.science/wp-admin/admin-ajax.php?action=h5p_embed&id=4" width="1407" height="866" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://blog.flavoursofopen.science/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

---

<p style="font-size:0.75em;"><a rel="license noopener noreferrer" href="http://creativecommons.org/licenses/by-sa/4.0/" data-wpel-link="external" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a> H5P-Timeline “<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Development of OER in Germany</span>” by <a xmlns:cc="http://creativecommons.org/ns#" href="https://blog.flavoursofopen.science/timeline-development-of-oer-in-germany/" property="cc:attributionName" rel="cc:attributionurl noopener noreferrer" data-wpel-link="external" target="_blank">Tobias Steiner</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY SA 4.0 International Lizenz</a>.</p>