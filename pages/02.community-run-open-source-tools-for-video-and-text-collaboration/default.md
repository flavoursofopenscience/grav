---
title: 'Community-run open source tools for video and text collaboration'
media_order: '20201122-edumeet-list-16-servers.txt,alina-grubnyak-R84Oy89aNKs-unsplash-cropped.jpg,bbb.png,eduMEET-small.png,jitsi-logo.png,etherpad.png,cryptpad.png,20201206-jitsi-list-260-servers.txt,codimd.png,unhangout-logo.png,Screenshot 2021-08-01 at 16-04-27-Excalidraw.png,excalidraw.png,20211223-jitsi-list-193-servers.txt,20211223-edumeet-list-12-servers.txt,20211226-jitsi-list-195-servers.txt'
published: true
date: '2020-11-14 15:32'
hide_hypothesis: false
sitemap:
    ignore: false
external_links:
    target: _blank
    mode: active
twitterenable: true
twittercardoptions: summary_large_image
twitterdescription: 'Updated collection of #opensource tools for video- and text-based collaboration (@jitsinews, @bigbluebutton, @EtherpadOrg, @CodiMD_Dev, @cryptpad, EduMeet). 99% of these are generously hosted by volunteer individuals and #nonprofits.'
articleenabled: true
personenabled: false
facebookenable: true
primaryImage:
    user/pages/09.community-run-open-source-tools-for-video-and-text-collaboration/alina-grubnyak-R84Oy89aNKs-unsplash-cropped.jpg:
        name: alina-grubnyak-R84Oy89aNKs-unsplash-cropped.jpg
        type: image/jpeg
        size: 790593
        path: user/pages/09.community-run-open-source-tools-for-video-and-text-collaboration/alina-grubnyak-R84Oy89aNKs-unsplash-cropped.jpg
hide_git_sync_repo_link: false
article:
    datePublished: '2020-11-14 15:32'
    dateModified: '2021-12-26 15:20'
    description: 'Updated collection of #opensource tools for video- and text-based collaboration (@jitsinews, @bigbluebutton, @EtherpadOrg, @CodiMD_Dev, @cryptpad, EduMeet). 99% of these are generously hosted by individual volunteers and small #nonprofit organizations.'
    image_url: /community-run-open-source-tools-for-video-and-text-collaboration/alina-grubnyak-R84Oy89aNKs-unsplash-cropped.jpg
    author: 'Tobias Steiner'
addperson:
    -
        person_name: null
        person_jobTitle: null
        person_address_addressLocality: null
        person_address_addressRegion: null
---

`updated: December 26, 2021`

_Please feel free to leave suggestions, critical notes, etc. via hypothes.is comments (see tab on the right-hand side of the screen) and I'll make sure to add these to the main text_

## Table of Contents

1. [BigBlueButton](#bigbluebutton) -- video alternative for teaching, learning, collaboration in projects, etc.
2. [EduMeet](#edumeet) --  alternative for video conferencing, collaboration in projects, etc.
3. [Unhangout](#unhangout) -- open source video/chat alternative focusing on group-based breakout sessions
5. [Misc. Open Source video alternatives for teaching](#misc)
6. [PeerTube](#peertube) -- open source alternative to video streaming platforms such as Youtube, Vimeo
7. [CryptPad](#cryptpad) -- text-based collaboration, alternative to GoogleDocs
8. [Etherpad](#etherpad) -- text-based collaboration / notetaking
9. [HackMD / CodiMD](#hedgedoc) -- Markdown-based collaborative writing / notetaking
10. [ExcaliDraw](#excalidraw) - open-source collaborative whiteboard tool
11. [Polling/Date finder/Scheduler](#poll) - open-source alternative to Doodle
12. [Jitsi Meet -- User- and/or community-run jitsi instances](#jitsi) -- video meeting / conferencing alternative to proprietary/commercial solutions such as Zoom
	* [jitsi Africa](#africa): [South Africa](#southafrica), [Tunisia](#tunisia)
	* [jitsi Asia](#asia): [India](#india), [Sri Lanka](#srilanka), [Thailand](#thailand)
	* [jitsi Europe](#europe): [Austria](#austria), [Belarus](#belarus), [Belgium](#belgium), [Czech Republic](#czechrepublic), [Finland](#finland), [France](#france), [Germany](#germany), [Italy](#italy), [Netherlands](#netherlands), [Portugal](#portugal), [Romania](#romania), [Russian Federation](#russianfederation), [Slovenia](#slovenia), [Spain](#spain), [Sweden](#sweden), [Switzerland](#switzerland), [UK / GB](#gb)
	* [jitsi South & North America](#south_north_america): [Canada](#canada), [United States](#unitedstates), [Uruguay](#uruguay), [Argentina](#argentina), [Chile](#chile), [El Salvador](#elsalvador)


## Open Source video alternative for teaching purposes: BigBlueButton <a id="bigbluebutton"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/bbb.png) BigBlueButton has a selection of useful features tailored for those teaching online seminars and classes. Next to that, we've also been using it for a variety of conferences, project meetings, etc.:

*     Shared whiteboard (multi-user enabled)
*     Shared notes (via minimalist text editor)
*     Public and private messaging options
*     Session recording support
*     Ability to group users for team collaboration via Breakout rooms
*     Polling options available
*     Screen sharing
*     Provides an API for easy integration on web applications
* 	  Frontend UI via Greenlight plugin, configurable for your own server.

#### Documentation:

* https://docs.bigbluebutton.org/

#### Community-run instances:

* https://fairteaching.net/b (fairkom)
* https://bbb.daten.reisen (y0sh / C4)
* https://senfcall.de
* https://bbb.opencloud.lu/b
* https://meeten.statt-drosseln.de/b
* https://bbb.cyber4edu.org
* https://bbb.hostsharing.net
* https://bbb.linxx.net (test install, but runs stable)
* https://bbb.stura.uni-heidelberg.de/b
* https://bbb.piratensommer.de/b (registration required, see notes)
* https://bbb.un-hack-bar.de/b (run by Chaostreff Unna)
* https://bbb.faimaison.net/b
* https://treffen.kbu.freifunk.net/ff
* https://bbb.bluit.de/b (Binary Kitchen e.V. - Hackspace Regensburg)
* https://videoconf.defis.info/b
* https://meeting.hostelyon.fr/b
* https://bbb.tbm.tudelft.nl/b
* https://bbb.wsweet.cloud/b
* https://blabla.aquilenet.fr/b
* https://visio.myceliandre.fr/b


## Misc. Open Source video alternatives for teaching <a id="misc"></a>

* https://app.classroomscreen.com/ (shared whiteboard with collab. apps to share links, comments, etc.)
* https://meetzi.de/ - Combination of Jitsi Meet, [Etherpad](https://etherpad.org/) and [Whiteboard](https://github.com/cracker0dks/whiteboard)
* https://www.org.meet.coop/ - "The Online Meeting Co-operative is a meeting and conferencing platform powered by BigBlueButton, stewarded by our Operational Members from across the world."

## Open Source video conferencing alternative: EduMeet  <a id="edumeet"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/eduMEET-small.png)   Alternative open-source approach to Jitsi Meet and BigBlueButton, based on WebRTC and mediasoup. Supported by GÈANT as part of the European Open Science Cloud scoping efforts, listed in [EOSCSynergy](https://indico.ifca.es/event/1613/contributions/7361/attachments/1175/1565/Wp6_slides_september.pptx.pdf).

📃 ➡️ Text file of the server list (2021-12-23, 12 servers): [20211223-edumeet-list-12-servers.txt](https://gitlab.com/flavoursofopenscience/grav/-/raw/master/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/20211223-edumeet-list-12-servers.txt)

* GitHub repo: https://github.com/edumeet/edumeet
* Landing page: https://edumeet.org
* Part of the [EOSC-Synergy](https://www.eosc-synergy.eu/) Learning Platform (in development): https://learn.eosc-synergy.eu/videoconference/

Description:
> In order to create a complete communication and fully functional solution that can meet the expectations of the scientific and research communities, enhanced functional features were designed, in particular:
>
> *     WebRTC audio/video communication;
> *     screen sharing, file sharing and chat feature;
> *     federated login, including eduGAIN;
> *     speaker detection (microphone analyser with visual indicator);
> *     audio and video streams management ;
> *     full-screen mode, raise hand option, connection testing;
> *     customizable view layout (democratic and filmstrip);
> *     high resolution support (tested up to 4K).


### France

| URL    | Run by   | SSL Test  | Server location |
|---|---|---|---|----|
| https://edu.colman.it   |  		 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=edu.colman.it&latest) | Online S.a.s.  |

### Germany

| URL    | Run by   | SSL Test  | Server location |
|---|---|---|---|----|
| https://meet.win-tek.eu  | Win-Tek	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.win-tek.eu&latest) | Contabo GmbH  |
| https://befair1.iorestoacasa.work  | BeFair	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=befair1.iorestoacasa.work&latest) | Hetzner Online GmbH  |
| https://iorestoacasa.operweb.cloud  | OperWEB by OperNEXT	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=iorestoacasa.operweb.cloud&latest) | Hetzner Online GmbH  |

### Italy

| URL    | Run by   | SSL Test  | Server location |
|---|---|---|---|----|
| https://mm.cedrc.cnr.it  |  Consiglio Nazionale delle Ricerche - Ufficio ICT-SAC  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=mm.cedrc.cnr.it&latest) | Consortium GARR  |
| https://edu.meet.garr.it  |  GARR  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=edu.meet.garr.it&latest) | Consortium GARR  |
| https://mm.rd.unidata.it  |  Unidata	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=mm.rd.unidata.it&latest)  |  UNIDATA S.p.A. |
| https://iorestoacasa02.cineca.it  |  	Cineca  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=iorestoacasa02.cineca.it&latest)   |  Consortium GARR |
| https://edumeeting.area.fi.cnr.it  |	Consiglio Nazionale delle Ricerche - Area di Ricerca di Firenze	 |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=edumeeting.area.fi.cnr.it&latest)    |  Consortium GARR |

### Netherlands

| URL    | Run by   | SSL Test  | Server location |
|---|---|---|---|----|
| https://edumeet.geant.org   |  	GÈANT	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=edumeet.geant.org&latest) |  MICROSOFT-CORP-MSN-AS-BLOCK  |

### Norway

| URL    | Run by   | SSL Test  | Server location |
|---|---|---|---|----|
| https://letsmeet.no  |  UNINETT	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=letsmeet.no&latest) | UNINETT AS  |

### Poland

| URL    | Run by   | SSL Test  | Server location |
|---|---|---|---|----|
| https://vc.learn.eosc-synergy.eu  |  EOSCSynergy	 | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=vc.learn.eosc-synergy.eu&latest) | Institute of Bioorganic Chemistry Polish Academy of Science, Poznan Supercomputing and Networking Center PSNC  |

----

## Open Source video / chat alternative: Unhangout (by MIT) <a id="unhangout"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/unhangout-logo.png)
[Description](https://unhangout.media.mit.edu/about/): Unhangout is an open source platform for running large-scale, participant-driven events online.

>Each event has a landing page, which we call the lobby. When participants arrive, they can see who else is there and chat with each other. Hosts can welcome their community and do introductions in a video window that gets streamed into the lobby. Participants can then join breakouts, which are small group video chats, for in-depth conversations, peer-to-peer learning, and collaboration on projects.

* Main URL: **[https://unhangout.media.mit.edu/](https://unhangout.media.mit.edu/)**
* Dev/secondary https://dev.unhangout.io
* See also: webinar series "Unhangout for Educators" https://u4e.media.mit.edu/

----
## Open Source alternative to video streaming platforms such as Youtube, Vimeo 

 <a id="peertube"></a>
 
 [PeerTube](https://joinpeertube.org/en_US/), developed by the good folks at FramaSoft, is a free software that, once installed on a server, generates a video hosting platform. This platform can be federated, to share its video catalog with other PeerTube platforms. It also provides a resilient video broadcasting system, which combines peer-to-peer and traditional streaming.
 
 A list of more than 900 instances that run Peertube can be found at: https://instances.joinpeertube.org/instances
 
 ---


## Open Source alternative to GoogleDocs, etc.: CryptPad <a id="cryptpad"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/cryptpad.png)   official: https://cryptpad.fr/

* community-hosted @ Freifunk Hamburg: https://pad.hamburg.freifunk.net
* community-hosted @ CCC Switzerland:  https://cryptpad.ccc-ch.ch
* community-hosted @ CCC Mannheim:    https://cryptpad.mannheim.ccc.de
* community-hosted @ Faelix: https://cryptpad.faelix.net
* community-hosted @ Digitalcourage: https://cryptpad.digitalcourage.de
* community-hosted @ MetaGer: https://cryptpad.metager.de
* community-hosted @ PiratenPartei: https://cryptpad.piratenpartei.de
* community-hosted @ RaspberryBlog: https://cryptpad.raspberryblog.de
* community-hosted @ UberSpace: https://cryptpad.uber.space
* community-hosted @ allmende.io: https://drive.allmende.io
* community-hosted @ CCC Wien: https://pads.c3w.at


💡 For more alternatives to Google services, see the excellent _DeGoogle_ list maintained by Josh Moore: https://degoogle.jmoore.dev/, and [GitHub repo](https://github.com/tycrek/degoogle)

----


## Open Source alternative to GoogleDocs for collaborative writing: Etherpad <a id="etherpad"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/etherpad.png)   ... see the [Etherpad Lite GitHub Wiki](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite) and https://etherpad.org for more details.

➡ make sure to also check the Etherpad Lite [list of sites that run Etherpad Lite](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite).

* provided by Etherpad Foundation with video chat option: https://video.etherpad.com
* community-hosted @ The Carpentries: https://pad.carpentries.org
* community-hosted @ RiseUp: https://pad.riseup.net
* community-hosted @ Wikimedia: https://etherpad.wikimedia.org
* community-hosted @ Disroot: https://pad.disroot.org
* community-hosted @ CCC: https://pads.ccc.de
* community-hosted @ CCC Aachen (C3AC): https://pads.aachen.ccc.de
* community-hosted @ CCC Dresden (C3D2): https://pentapad.c3d2.de
* community-hosted @ CCC Mannheim (C3MA): https://pad.ccc-mannheim.de
* community-hosted @ Systemli.org: https://pad.systemli.org
* community-hosted @ fairkom (AT): https://board.net
* community-hosted @ Framasoft: https://framapad.org/en
* community-hosted @ Infini: https://pad.infini.fr
* community-hosted @ Picasoft: https://pad.picasoft.net
* community-hosted @ German Federal Youth Council DBJR / jugend.beteiligen.jetzt - für die Praxis digitaler Partizipation: https://yopad.eu
* community-hosted @ Mafiasi (Informatik Fachschaft UHH):   https://ep.mafiasi.de
* community-hosted @ Piratenpartei Sachsen: https://pad.piratensommer.de
* community-hosted @ Informatik FU Berlin: https://pad.spline.inf.fu-berlin.de
* community-hosted @ Studi-Fachschaften RWTH Aachen: https://fachschaften.rwth-aachen.de/etherpad
* community-hosted @ ASTA Uni Osnabrück: https://www.asta.uni-osnabrueck.de/etherpad/
* community-hosted @ Snopyta: https://etherpad.snopyta.org/



----


## Open Source alternative to GoogleDocs for markdown-based collaborative writing: HackMD/CodiMD, now HedgeDoc <a id="hedgedoc"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/codimd.png)

* SAAS: HackMD https://hackmd.io/
* FOSS fork: CodiMD, GitHub repo: https://github.com/hackmdio/codimd
* Documentation: https://hackmd.io/c/codimd-documentation
* **CodiMD renamed to Hedgedoc**: https://community.codimd.org/t/codimd-becomes-hedgedoc/170
* Hedgedoc fork: [GitHub repo](https://github.com/hedgedoc/hedgedoc) - SAAS: HedgeDoc https://demo.hedgedoc.org/
* hosted by GWDG with federated login for the German HEI community (login required): https://pad.gwdg.de/
* Communtiy-run instances @ (use "new guest note"):
	* https://md.bytewerk.org/
	* https://md.inf.tu-dresden.de/
	* https://codimd.mathphys.stura.uni-heidelberg.de/
	* https://pad.lamyne.org/

----

## Open source collaborative whiteboard: Excalidraw <a id="excalidraw"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/excalidraw.png)

> Virtual whiteboard for sketching hand-drawn like diagrams

Official: https://excalidraw.com
GitHub: https://github.com/excalidraw/excalidraw
Twitter: [@excalidraw](https://twitter.com/excalidraw)

----

## Misc. Open Source Date-Finding / Polling alternatives to Doodle <a id="poll"></a>

#### Framadate

An open-source polling/scheduling tool focusing on privacy, run by French non-profit Framasoft

**Source**: https://framadate.org

* community-hosted @ GC Project: https://framadate.ggc-project.de
* community-hosted @ DigitalCourage e.V.: https://nuudel.digitalcourage.de
* community-hosted @ Disroot: https://poll.disroot.org
* community-hosted @ RollenspielMonster: https://poll.rollenspiel.monster
* community-hosted @ Snopyta: https://poll.snopyta.org
* community-hosted @ Verein zur Förderung eines Deutschen Forschungsnetzes e.V. (DFN): https://terminplaner4.dfn.de

----


## User- and/or community-run jit.si instances <a id="jitis"></a>

![](https://flavoursofopen.science/user/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/jitsi-logo.png)   as decentralized additive approach to lessen the load on https://meet.jit.si/

You can use [DNS Lookup](https://www.ultratools.com/tools/dnsLookupResult), [GeoIP Lookup](https://www.ultratools.com/tools/geoIpResult), and [KeyCDN Geo Tool](https://tools.keycdn.com/geo) to find out more about each server's location.

## ✨✨ Official Servers

* 🔗 https://meet.jit.si/ -- 📞
* 🔗 https://8x8.vc/


## ✨🧰 User- and/or community-run jit.si instances by country

*(feel free to add other servers you know of via hypothes.is comments (see commentary feature on the right-hand side)*

📃 ➡️  Server list, text file: [2021-12-26, 195 servers](https://gitlab.com/flavoursofopenscience/grav/-/raw/master/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/20211226-jitsi-list-195-servers.txt)

➡️ ➡️ ... and the [Community Wiki on GitHub](https://jitsi.github.io/handbook/docs/community/community-instances)

🖥️ ➡️ **Jitsi Monitor** "... monitors all known public Jitsi Meet instances for ping times and TLS quality": [Guardianproject/JitsiMonitor](https://guardianproject.gitlab.io/jitsi-monitor/) and [JitsiMonitor on GitLab](https://gitlab.com/guardianproject/jitsi-monitor)

🌟🖥️ ➡️ **Jitsi Meter** "... Helping with the decision of which instance of Jitsi to use. An list of public Jitsi Meet instances sorted for ping times and TLS quality in relation to the user's location: [JitsiMeter](https://ladatano.partidopirata.com.ar/jitsimeter/) and [JitsiMeter Repo](https://0xacab.org/faras/jitsimeter)

🖥️ ➡️🖥️ **Jitsi Admin** - central admin and management interface for Jitsi server setups. [Project website](https://jitsi-admin.de/) and [GitHub repo](https://github.com/H2-invent/jitsi-admin)

🌟🌟 **special feature: auto-forwarding to a random jitsi-Server** -- **[jitsi.random-redirect.de](https://jitsi.random-redirect.de)** ----- How does it work? check out the [GitHub repo](https://github.com/tosterkamp/random-redirect)

📲📲 Next to its native webclient interface, Jitsi Meet is now also available as
* an open-source **Jitsi desktop app**, see [Jitsi Meet Electron for Desktop](https://github.com/jitsi/jitsi-meet-electron/) (Windows, Mac, and Linux clients available)
* a **mobile app**, find it in the [Google Play](https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=fr) Store, the [Apple Store](https://itunes.apple.com/us/app/jitsi-meet/id1165103905?mt=8), or the [F-Droid Store](https://f-droid.org/en/packages/org.jitsi.meet/).

## Europe <a id="europe"></a>

### Austria <a id="austria"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|
| easyconference.uibk.ac.at  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=easyconference.uibk.ac.at&latest) | University of Innsbruck  |
| meet.epicenter.works  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.epicenter.works&latest) |  Nessus GmbH  |
| fairmeeting.net | ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=fairmeeting.net&latest) |  |
| pro.fairmeeting.net | ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=pro.fairmeeting.net&latest) |  |
| meet.graz.social  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.graz.social&latest) |    |

### Belarus <a id="belarus"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| meet.naveksoft.com |  ✅   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.naveksoft.com&latest) |  Navek Soft & Mobile Service LTD  |

### Belgium <a id="belgium"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jitsi-1.belnet.be |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi-1.belnet.be&latest) | BELNET |
| praatbox.be |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=praatbox.be&latest) |  |


### Czech Republic <a id="czechrepublic"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jitsi.ssps.cz |  ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.ssps.cz&latest) | Futruy s.r.o. |
| meet.vpsfree.cz | ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.vpsfree.cz&latest) |  Master Internet s.r.o.  |



### Finland <a id="finland"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jitsi.riot.im |  ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.riot.im&latest) | UpCloud Ltd, hosted by New Vector on behalf of the Matrix.org Foundation |


### France (partly sourced via [Framasoft/Framatalk](https://framatalk.org/accueil/fr/info/)) <a id="france"></a>


| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|
|  	allo.bim.land	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=allo.bim.land&latest)	   |  	ILOTH	  |
|  	blabla.dedidream.com	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=blabla.dedidream.com&latest)	   |  	Online S.a.s.
|  	conf.conf.domainepublic.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=conf.domainepublic.net&latest)	   |  	Bouygues Telecom SA
|  	conf.underworld.fr	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=conf.underworld.fr&latest)	   |  	Bouygues Telecom SA
|  meet.kbu.freifunk.net |    |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.kbu.freifunk.net&latest) | Online S.a.s. |
|  suricate.tv |    |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=suricate.tv&latest) | Online S.a.s. |
|  jump.chat |    |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jump.chat&latest) | Online S.a.s. |
|  	framatalk.grifon.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=framatalk.grifon.fr&latest)	   |  	Association GRIFON
|  	jitsi.cheezecake.ovh	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.cheezecake.ovh&latest)	   |  	Online S.A.S.
|  	jitsi.hadoly.fr	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hadoly.fr&latest)	   |  	Grenode
|  	jitsi.hivane.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hivane.net&latest)	   |  	Hivane
|  	jitsi.milkywan.fr	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.milkywan.fr&latest)	   |  	MilkyWan
|  	jitsi.nextmap.io	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.nextmap.io&latest)	   |  	NXT Initiative SAS
|  	jitsi.q2r.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.q2r.net&latest)	   |  	Online S.a.s.
|  	jitsi.tetaneutral.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.tetaneutral.net&latest)	   |  	Tetaneutral.net
|  	jitsi.videodulib.re	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.videodulib.re&latest)	   |  	Tetaneutral.net
|  	meet.artifaille.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.artifaille.fr&latest)	   |  	 
|  	meet.digdeo.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.digdeo.fr&latest)	   |  	OVH SAS
|  	meet.evolix.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.evolix.org&latest)	   |  	Evolix SARL
|  	meet.lenalio.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.lenalio.fr&latest)	   |  	Oceanet Technology SAS
|  	meet.nevers-libre.org	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.nevers-libre.org&latest)	   |  	Netrix SAS
|  	meet.tedomum.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.tedomum.net&latest)	   |  	Online S.a.s.
|  	meet.yapbreak.fr	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.yapbreak.fr&latest)	   |  	OVH SAS
|  	meeting.rezopole.net	   |  	 ⁉	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meeting.rezopole.net&latest)	   |  	Rezopole A.D.
|  	rendez-vous.renater.fr	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=rendez-vous.renater.fr&latest)	   |  	Renater
|   jitsi.debamax.com   | ✅   |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.debamax.com&latest) | Online S.a.s. |
|  	talk.fdn.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=talk.fdn.fr&latest)	   |  	Association Gitoyen
|  	talk.ouvaton.coop	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=talk.ouvaton.coop&latest)	   |  	Inulogic Sarl
|  	avecvous.linagora.com	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=avecvous.linagora.com&latest)	   |  	 |
|  	jitsi.laas.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.laas.fr&latest)	   |  	 |
|  	video.devloprog.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=video.devloprog.org&latest)	   |  	Bouygues Telecom SA
|  	video.omicro.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=video.omicro.org&latest)	   |  	OVH SAS
|  	jitsi.komuniki.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.komuniki.fr&latest)	   |  	OVH SAS
|  	visio.lafranceinsoumise.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=visio.lafranceinsoumise.fr&latest)	   |  	Online S.a.s.
|  	webcall.paulla.asso.fr	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=webcall.paulla.asso.fr&latest)	   |  	Axione S.a.s.
|  	webconf.viviers-fibre.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=webconf.viviers-fibre.net&latest)	   |  	OVH SAS
|  	vidconf.tech4good.ch	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=vidconf.tech4good.ch&latest)	   |

#### special cases (limited access)

*    https://rendez-vous.renater.fr
*    https://webconf.numerique.gouv.fr


### Germany <a id="germany"></a>

➡️ ➡️ *for a subset collected by the folks at Chaos Computer Club, see [pads.ccc.de/jitsiliste](https://pads.ccc.de/jitsiliste)*


| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|
| meet.ffmuc.net | ✅  |  ❌ (only Frontend no videotraffic through CF)  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.ffmuc.net&latest) | CLOUDFLARENET  |
| meet.teamcloud.work | ✅ | | [SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.teamcloud.work&latest) | Hetzner Online GmbH
|  	besprechung.net	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=besprechung.net&latest)	   |  	Host Europe GmbH	  |
|  	meet.physnet.uni-hamburg.de   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.physnet.uni-hamburg.de&latest)	   |  	|
|  	meet.in-berlin.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.in-berlin.de&latest)	   |  	 	  |
|  	meet.jotbe.io	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.jotbe.io&latest)	   |  	 	  |
|  	meet.nerd.re	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.nerd.re&latest)	   |  	Contabo GmbH	  |
|  	fi-hel-1.jitsi.rocks	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=fi-hel-1.jitsi.rocks&latest)	   |  	Hetzner Online GmbH	  |
|  	fi-hel-2.jitsi.rocks	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=fi-hel-2.jitsi.rocks&latest)	   |  	Hetzner Online GmbH	  |
|  	framatalk.org	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=framatalk.org&latest)	   |  	Hetzner Online GmbH	  |
|  	freejitsi01.netcup.net	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=freejitsi01.netcup.net&latest)	   |  	netcup GmbH	  |
|  	jitsi.amikoop.coop	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.amikoop.coop&latest)	   |  	Contabo GmbH	  |
|  	jitsi.arl.wtf	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.arl.wtf&latest)	   |  	fux eG	  |
|  	jitsi.debian.social	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.debian.social&latest)	   |  	Hetzner Online GmbH	  |
|  	jitsi.dorf-post.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.dorf-post.de&latest)	   |  		  |
|  	jitsi.fem.tu-ilmenau.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.fem.tu-ilmenau.de&latest)	   |  	DFN	  |
|  	jitsi.flyingcircus.io	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.flyingcircus.io&latest)	   |  	KAMP Netzwerkdienste GmbH	  |
|  	jitsi.freifunk-duesseldorf.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.freifunk-duesseldorf.de&latest)	   |  	Freifunk Duesseldorf e.V.	  |
|  	jitsi.hamburg.ccc.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hamburg.ccc.de&latest)	   |  	Hetzner Online GmbH	  |
|  	jitsi.hs-anhalt.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hs-anhalt.de&latest)	   |   	  |
|  	jitsi.komun.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.komun.org&latest)	   |  	Contabo GmbH	  |
|  	jitsi.linux.it	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.linux.it&latest)	   |  	Hetzner Online GmbH	  |
|  	jitsi.netways.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.netways.de&latest)	   |  	NETWAYS GmbH	  |
|  	jitsi.osna.social	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.osna.social&latest)	   |  	Verein zur Foerderung eines Deutschen Forschungsnetzes e.V. 	  |
|  	jitsi.piratenpartei-duesseldorf.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.piratenpartei-duesseldorf.de&latest)	   |  	Contabo GmbH	  |
|  	jitsi.sixtopia.net	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.sixtopia.net&latest)	   |  	Alexej Senderkin	  |
|  	jitsi.uni-kl.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.uni-kl.de&latest)	   |  	    Technische Universitaet Kaiserslautern	  |
|  	jitsi.v3g.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.v3g.de&latest)	   |  	Liberty Global B.V.	  |
|  	jitsi.zfn.uni-bremen.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.zfn.uni-bremen.de&latest)	   |  	DFN	  |
|  	konferenz.netzbegruenung.de	   |  	 ✅ -- 📞	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=konferenz.netzbegruenung.de&latest)	   |  	Hetzner Online GmbH	  |
|  	www.kuketz-meet.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=kuketz-meet.de&latest)	   |  	netcup GmbH	  |
|  	meet.adminforge.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.adminforge.de&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.alpha.berlin	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.alpha.berlin&latest)	   |  	manitu GmbH	  |
|  	meet.alps.one	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.alps.one&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.cybercyber.digital	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.cybercyber.digital&latest)	   |  	Hetzner Online GmbH	  |
|  	jitsi.stura.htwk-leipzig.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.stura.htwk-leipzig.de&latest)	   |  	DFN	  |
|  	meet.darmstadt.social	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.darmstadt.social&latest)	   |  	netcup GmbH	  |
|  	meet.ffrn.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.ffrn.de&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.ffbrb.de   |  		 ✅	 	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.ffbrb.de&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.freifunk-aachen.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.freifunk-aachen.de&latest)	   |  	RelAix Networks GmbH	  |
|  	meet.freifunk-rhein-sieg.net	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.freifunk-rhein-sieg.net&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.golem.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.golem.de&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.hackerspace-bremen.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.hackerspace-bremen.de&latest)	   |  	  |
|  	meet.hasi.it	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.hasi.it&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.isf.es   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.isf.es&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.jugendrecht.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.jugendrecht.org&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.kobschaetzki.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.kobschaetzki.de&latest)	   |  	myLoc managed IT AG	  |
|  	meet.lrz.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.lrz.de&latest)	   |  	Leibniz-Rechenzentrum	  |
|  	meet.lug-stormarn.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.lug-stormarn.de&latest)	   |  		  |
|  	meet.meerfarbig.net	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.meerfarbig.net&latest)	   |  	meerfarbig GmbH & Co. KG	  |
|  	meet.no42.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.no42.org&latest)	   |  	1&1 Ionos Se	  |
|  	meet.opensuse.org	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.opensuse.org&latest)	   |  	 	  |
|  	meet.piraten-witten.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.piraten-witten.de&latest)	   |  	netcup GmbH	  |
|  	meet.piratenpartei-sh.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.piratenpartei-sh.de&latest)	   |  	ISPpro Internet KG	  |
|  	meet.roflcopter.fr	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.roflcopter.fr&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.rollenspiel.monster	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.rollenspiel.monster&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.scheible.it	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.scheible.it&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.schnuud.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.schnuud.de&latest)	   |  	Hetzner Online GmbH	  |
|  	meet.stura.uni-heidelberg.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.stura.uni-heidelberg.de&latest)	   |  		  |
|  	meet.systemli.org	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.systemli.org&latest)	   |  	Digitale Partizipation e.V.	  |
|  	meet.techkids.org	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.techkids.org&latest)	   |  	  |
|  	meet.weimarnetz.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.weimarnetz.de&latest)	   |  	Foerderverein Freie Netzwerke e.V.	  |
|  	teamjoin.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=teamjoin.de&latest)	   |  	Hetzner Online GmbH	  |
|  	talk.snopyta.org	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=talk.snopyta.org&latest)	   |  	Hetzner Online GmbH	  |
|  	vc.autistici.org	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=vc.autistici.org&latest)	   |  	Hetzner Online GmbH	  |
|  	viko.extinctionrebellion.de	   |  	 ✅	   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=viko.extinctionrebellion.de&latest)	   |  	SysEleven GmbH	  |
|  	webmeeting.opensolution.it	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=webmeeting.opensolution.it&latest)	   |  	DIGITALOCEAN-ASN	  |
|  	virtual.chaosdorf.space	   |  		   |  		   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=virtual.chaosdorf.space/Lounge&latest)	   |  		   |
| jitsi.eichstaett.social |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.eichstaett.social&latest)	   |  	Severloft GmbH	   |
| konferenz.buehl.digital |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=konferenz.buehl.digital&latest)	   |  	   |
| meet.stuvus.uni-stuttgart.de |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.stuvus.uni-stuttgart.de&latest)	   | Universität Stuttgart 	   |
| jitsi.hs-anhalt.de |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hs-anhalt.de&latest) | Hetzner Online GmbH |
| custom.jitsi.rocks |    |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=custom.jitsi.rocks&latest) | Hetzner Online GmbH |
| jitsi.php-friends.de |    |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.php-friends.de&latest) | PhP-Friends GmbH |
| meet.studiumdigitale.uni-frankfurt.de |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.studiumdigitale.uni-frankfurt.de&latest) | Johann Wolfgang Goethe-Universitat Frankfurt am Main |
| onlinetreff.ash-berlin.eu |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hs-anhalt.de&latest) | netcup GmbH |
| jitsi-meet.online |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi-meet.online&latest) | Hetzner Online GmbH |
| meet.nerd.re  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.nerd.re&latest) | Hetzner Online GmbH |
| meet.bjoernhaendler.de |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.bjoernhaendler.de&latest) | netcup GmbH |
| jitsi.mpi-bremen.de  |  ✅ - 📞  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.mpi-bremen.de&latest) | Verein zur Foerderung eines Deutschen Forschungsnetzes e.V.  |
| meet.rdi.zimt.uni-siegen.de  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.rdi.zimt.uni-siegen.de&latest) | Verein zur Foerderung eines Deutschen Forschungsnetzes e.V.  |
| meet.ur.de  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.ur.de&latest) | Verein zur Foerderung eines Deutschen Forschungsnetzes e.V.  |
| jitsi-01.csn.tu-chemnitz.de  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi-01.csn.tu-chemnitz.de&latest) | Verein zur Foerderung eines Deutschen Forschungsnetzes e.V.  |
| jitsi.folkwang-uni.de  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.folkwang-uni.de&latest) | Verein zur Foerderung eines Deutschen Forschungsnetzes e.V.  |
| jitsi.hs-nb.de  |  ✅ |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hs-nb.de&latest) | Verein zur Foerderung eines Deutschen Forschungsnetzes e.V.  |

#### special cases (limited access, mostly HEI)

*    https://jitsi.zfn.uni-bremen.de -- DFN (valid Uni Bremen account is required for creating new rooms)
*    https://vct.testsrv.gwdg.de -- DFN (valid GWDG account is required for creating new rooms)
*    https://jitsi.tu-dresden.de -- DFN (valid TU Dresden account is required for creating new rooms)
*    https://meet.b-tu.de -- DFN (valid BTU Cottbus-Senftenberg account is required for creating new rooms)
*    https://meet.lrz.de/ -- valid Münchner Wissenschaftsnetz account is required for creating new rooms


### Italy <a id="italy"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| video.beic.it |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=video.beic.it&latest) |  Consortium GARR |
| jitsi.area.fi.cnr.it	 |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.area.fi.cnr.it&latest)  | Consortium GARR  |
| jitsi.cedrc.cnr.it |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.cedrc.cnr.it&latest)  | Consortium GARR  |
| iorestoacasa01.cineca.it	 |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=iorestoacasa01.cineca.it&latest)  | Consortium GARR  |
| smaug.lixper.it |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=smaug.lixper.it&latest)  | 	Telecom Italia	|
| iorestoacasa.unicam.it  |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=iorestoacasa.unicam.it&latest)  | Consortium GARR  |
| meet.a80.it  |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.a80.it&latest)  | Consortium GARR  |
| balin.siriustec.it 	 |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=balin.siriustec.it&latest)  | Sirius Technology SRL |
| open.meet.garr.it		 |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=open.meet.garr.it&latest)  | Consortium GARR |

### Netherlands <a id="netherlands"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| beeldbellen.vc4all.nl |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=beeldbellen.vc4all.nl&latest) | Transip B.V. |
| jitsi.tuxis.net |  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.tuxis.net&latest) |  |
| meet.greenmini.host |  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.greenmini.host&latest) |  |
| meetme.bit.nl |  ✅  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meetme.bit.nl&latest) | BIT BV  |
| calls.disroot.org |  ✅  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=calls.disroot.org&latest) | Serverius Holding B.V.  |
| jitsi.hivos.org |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hivos.org&latest) | WorldStream B.V.  |
| meet.waag.org |  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.waag.org&latest) |  |
| jitsi.nluug.nl |  ✅  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.nluug.nl&latest) |  ProcoliX B.V. |
| vdc.dyne.org |    |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=vdc.dyne.org&latest) |  |
| meet.greenhost.net |    |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.greenhost.net&latest) |  |
| meet.domov.de	   |  		   |  	   	   |  	[SSL check](https://www.ssllabs.com/ssltest/analyze.html?d=meet.domov.de&latest)	   |  RouteLabel V.O.F.	  |
| meet.speakup.nl |    |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.speakup.nl&latest) |  |
| video.linxx.net |  ✅    |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=video.linxx.net&latest) |  Hetzner Online GmbH  |


### Portugal <a id="portugal"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jitsipodcast.linuxtech.pt |  ✅    |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsipodcast.linuxtech.pt&latest) |     |


### Romania <a id="romania"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
|   |   |   |   |    |


### Russian Federation <a id="russianfederation"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| conf.edu-kuban.ru |   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=conf.edu-kuban.ru&latest) |   |
| jitsi.ufanet.ru |   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.ufanet.ru&latest) |   |

### Slovenia <a id="slovenia"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
|   |     |   |     |    |


### Spain <a id="spain"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| meet.guifi.net | ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.guifi.net&latest) | Fundacio Privada per a la Xarxa Lliure, Oberta i Neutral, guifi.net  |
| videoconferencia.valenciatech.com | ✅   |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=videoconferencia.valenciatech.com&latest) | VALENCIATECH - Servicios de Informática con Software Libre - Administración y provisión de servidores GNU/Linux.  |

### Sweden <a id="sweden"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jitsi.brainmill.com |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.brainmill.com&latest) | Stiftelsen Chalmers Studenthem |
| meet.operationtulip.com |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.operationtulip.com&latest) | Bahnhof AB  |

### Switzerland <a id="switzerland"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| open.meet.switch.ch | ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=open.meet.switch.ch&latest) | SWITCH |
| swiss-meet.hidora.com |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=swiss-meet.hidora.com&latest) | HIDORA |
| vidconf.tech4good.ch |  ✅  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=vidconf.tech4good.ch&latest) | VTX Services SA  |
| www.free-solutions.org | ✅ |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=www.free-solutions.org&latest) | VTX Services SA  |
| swisschat.free-solutions.org | ✅  |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=swisschat.free-solutions.org&latest) | VTX Services SA |
| tel.free-solutions.org | ✅ |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=tel.free-solutions.org&latest) | VTX Services SA  |
| eelo.free-solutions.org | ✅ |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=eelo.free-solutions.org&latest) | VTX Services SA |
| noalyss.free-solutions.org | ✅  |   | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=noalyss.free-solutions.org&latest) | VTX Services SA |
| meet.infomaniak.com |  |  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.infomaniak.com&latest) |  |
| jitsi.math.uzh.ch |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.math.uzh.ch&latest) | SWITCH |
| jitsi.ff3l.net |  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.ff3l.net&latest) | Init7 |
| unibe.meet.switch.ch | ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=unibe.meet.switch.ch&latest) | SWITCH |
| unifr.meet.switch.ch | ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=unifr.meet.switch.ch&latest) | SWITCH |
| uzh.meet.switch.ch | ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=uzh.meet.switch.ch&latest) | SWITCH |
| meet-7.immerda.ch |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet-7.immerda.ch&latest) |  |
| hosttech.chat |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=hosttech.chat&latest) |  |
| meet.cyon.tools |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.cyon.tools&latest) |  |
| meet.init7.net |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.init7.net&latest) | Init 7 AG |
| meet.hostpoint.ch |   |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.hostpoint.ch&latest) | Hostpoint AG |
| meet.coredump.ch |  ✅  |  | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.coredump.ch&latest) | Nine Internet Solutions AG  |

### UK / GB <a id="gb"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
|   |    |   |   |    |



## South & North America <a id="south_north_america"></a>


### Argentina <a id="argentina"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jitsi.unp.edu.ar | ✅    |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.unp.edu.ar&latest) |   Red de Interconexion Universitaria  |
| jitsi.uner.edu.ar |  ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.uner.edu.ar&latest) |   Universidad Nacional de Entre Rios   |
| jitsi.hostcero.com |    |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.hostcero.com&latest) | Servicios y Telecomunicaciones S.A.  |
| jitsi.justiciajujuy.gov.ar |    |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.justiciajujuy.gov.ar&latest) |  Telecom Argentina S.A.  |


### Canada <a id="canada"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| conference.facil.services |  ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=conference.facil.services&latest) | OVH SAS  |
| appels.dev.facil.services |  ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=appels.dev.facil.services&latest) | OVH SAS  |
| meet-6.immerda.ch |  ✅   |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet-6.immerda.ch&latest) | KOUMBIT  |

### Chile <a id="chile"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| vc.sanclemente.cl |    |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=vc.sanclemente.cl&latest) |  ENTEL CHILE S.A. |

### El Salvador <a id="elsalvador"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| jmeet.uca.edu.sv  |    |  ❌  |   [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jmeet.uca.edu.sv&latest)    |  GOOGLE  |

### United States <a id="unitedstates"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| gruveo.com | ✅   |  ❌  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=gruveo.com&latest) | AMAZON-AES  |
| meet.jit.si |    |  ❌  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.jit.si&latest) | AMAZON-02  |
| meet.mayfirst.org |  ✅  |    |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.mayfirst.org&latest) | HURRICANE  |
| team.video |  ✅  |    |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=team.video&latest) | HURRICANE  |
| swrtc.talky.io |   |  ❌ | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=swrtc.talky.io&latest) | AMAZON-02 |
| jitsi.member.fsf.org/ | ✅ | | [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=jitsi.member.fsf.org&latest) | Free Software Foundation |


### Uruguay <a id="uruguay"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
|   |    |   |   |  |



## Africa <a id="africa"></a>

### South Africa <a id="southafrica"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| noumeet.com |  ✅    | ❌  |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=noumeet.com&latest) |  Teraco Johannesburg  |

### Tunisia <a id="tunisia"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
|   |    |   |   |   |

## Asia <a id="asia"></a>

### India <a id="india"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
|   |    |   |   |   |

### Sri Lanka  <a id="srilanka"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|
| meet.gov.lk |     |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=meet.gov.lk&latest) | Information and Communication Technology Agency of Sri Lanka |

### Thailand <a id="thailand"></a>

| URL    | Non-Google STUN/TURN  | On Amazon, Google, Cloudflare or Microsoft   | SSL Test  | hoster |
|---|---|---|---|---|---|---|
| vroom.truevirtualworld.com |    |   |  [run SSL check!](https://www.ssllabs.com/ssltest/analyze.html?d=vroom.truevirtualworld.com&latest) |   |

-----


## Some additional bits & pieces ...

* (in German): https://www.golem.de/news/homeoffice-videokonferenzen-auf-eigenen-servern-mit-jitsi-meet-2003-147239.html
* Why open infrastructure? https://flavoursofopen.science/teaching-practices-and-values-of-open-science-and-scholarship-needs-open-infrastructure
* (in German) Jitsi und Big Blue Button beim Deutschlandfunk - https://www.deutschlandfunk.de/jitsi-und-big-blue-button-open-source-angebote-fuer.684.de.html?dram:article_id=476375

### Jitsi use advice:
* use chromium / Chrome -- for those not wanting to install Google Chrome, you might consider using [Iridium](https://iridiumbrowser.de/) or [Brave](https://brave.com/) instead.
* if not really necessary, have most participants use audio-only, in many settings it is not necessary that all people are visible. Turning video off helps safe bandwith


### Jitsi setup advice:
* Base on Github: https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md
* hosting on AWS: https://dev.to/noandrea/self-hosted-jitsi-server-with-authentication-ie7
    * https://aws.amazon.com/blogs/opensource/getting-started-with-jitsi-an-open-source-web-conferencing-solution/
* CentOS: https://wiki.opensourceecology.org/wiki/Jitsi
* Ubuntu 18.04 LTS: https://www.howtoforge.com/tutorial/how-to-create-your-own-video-conference-using-jitsi-meet-on-ubuntu-1804/
* Docker: https://github.com/jitsi/docker-jitsi-meet
* ...

===

> Photo by [Alina Grubnyak](https://unsplash.com/@alinnnaaaa?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
