---
title: About
media_order: 'e-corp.gif,noimage.jpg,philipp-deus-lEZ4J7qIQdA.jpg,1607339517-2.pdf,2020.pdf'
published: true
hide_hypothesis: false
external_links:
    target: _blank
    mode: active
twitterenable: true
twittercardoptions: summary
articleenabled: false
personenabled: false
facebookenable: true
primaryImage:
    user/pages/01.home/philipp-deus-lEZ4J7qIQdA.jpg:
        name: philipp-deus-lEZ4J7qIQdA.jpg
        type: image/jpeg
        size: 887466
        path: user/pages/01.home/philipp-deus-lEZ4J7qIQdA.jpg
fontawesome: use_global
animate_css: use_global
wow_js: use_global
google_prettify: use_global
foundation_abide_js: use_global
foundation_accordion_js: use_global
foundation_accordionMenu_js: use_global
foundation_drilldown_js: use_global
foundation_dropdown_js: use_global
foundation_dropdownMenu_js: use_global
foundation_equalizer_js: use_global
foundation_interchange_js: use_global
foundation_magellan_js: use_global
foundation_offcanvas_js: use_global
foundation_orbit_js: use_global
foundation_responsiveMenu_js: use_global
foundation_responsiveToggle_js: use_global
foundation_reveal_js: use_global
foundation_slider_js: use_global
foundation_sticky_js: use_global
foundation_tabs_js: use_global
foundation_toggler_js: use_global
foundation_tooltip_js: use_global
foundation_util_box_js: use_global
foundation_util_keyboard_js: use_global
foundation_util_mediaQuery_js: use_global
foundation_util_motion_js: use_global
foundation_util_nest_js: use_global
foundation_util_timerAndImageLoader_js: use_global
foundation_util_touch_js: use_global
foundation_util_triggers_js: use_global
meta_robots:
    index: true
    follow: true
    noindex: false
    nofollow: false
    noimageindex: false
override_default_title: 'About me | Flavours of Open'
override_default_desc: 'Welcome to Flavours of Open! This here is my little sandbox - a playground that I use as a site of experimentation for exploring #opensource tools and modes of presentation for #openscience and #openeducation... in short: to test what works for me, and what doesn''t. Much of that involves toying around with [Grav](https://getgrav.org/), [WordPress](https://blog.flavoursofopen.science), [H5P](https://h5p.org/), and other stuff...'
override_default_img: 'philipp-deus-lEZ4J7qIQdA.jpg?g-6a72250a$'
---

   
##  Welcome to _**Flavours of Open**_!

This here is my little sandbox - a playground that I use as a site of experimentation for exploring #opensource tools and modes of presentation for #openscience and #openeducation... in short: to test what works for me, and what doesn't. Much of that involves toying around with [Grav](https://getgrav.org/), [WordPress](https://blog.flavoursofopen.science), [H5P](https://h5p.org/), and other stuff... 

#### About

"Me" in this particular case is Tobias Steiner. I have studied in Hamburg and London and hold an MA in Television Studies from Birkbeck, University of London. Between 2013 and 2019, I've been working as a research fellow in the fields of open source, open science and open education at Universität Hamburg. As of February 2020, I have now joined the [COPIM](https://www.copim.ac.uk/) project (short for "Community-led Open Publication Infrastructures for Monographs") as a project manager.

I consider myself a traveler between the worlds of advocacy for Openness in its many facets, mixed with a little bit of project management (see work experience), and a distinct dash of Cultural and Media Studies as academic background - the field in which I've pursued a part-time PhD for more than five years, but ultimately decided to quit in the early months of 2018 due to personal reasons. 

My list of publications reflects this perceived multiplicity of areas, with blog posts, articles and book chapters ranging from questions of authorship in _Game of Thrones_, over transnational renegotiations of Nordic Noir (in print), to a variety of thoughts and musings from the intersection of Open Education,Open Source, Open Publishing, Science and Scholarship. 

If you want to find out more about what I do, why not briefly pay a visit to the following sites & communities - it's very likely that you'll find me there in one way or the other :)

* My ORCiD: [0000-0002-3158-3136](https://orcid.org/0000-0002-3158-3136)
* Twitter: [@cmplxtv_studies](https://twitter.com/cmplxtv_studies)
* My [Zotero library](https://www.zotero.org/flavorsofopenscience/library)
* OATP TagTeam feed: https://tagteam.harvard.edu/hubs/oatp/user/flavoursofopenscience
* Blog for H5P-related stuff, workshop & presentation material, timelines, etc. ... : [blog.flavoursofopen.science](https://blog.flavoursofopen.science)
* plus, subscribe to The Open Community Calendar: [cal.opensciencemooc.eu](https://cal.opensciencemooc.eu)

And if you want to send me a few lines about the current (non-)state of things or on any other subject / matter, please do get in touch via email: **_info-at-flavoursofopen.science_**


Please stay tuned & check back in the next few weeks!

---

... and on another note:

### [Open Scholarship](https://doi.org/10.32388/282729) (my preferred definition):

Open Scholarship as a paradigm consists of three dimensions:

1. **Open output**, comprising specific content / products from the fields of open publishing, open data, open source, open education, open access, and similar open movements.

2. **Open practices**, describing how those working in and with academia search and find, (co-)create and disseminate the elements of 1), such as e.g. open science practices for research-focused activities; open teaching and learning in its many facets including open educational practices and open pedagogy; citizen science; scholarly communication, outreach and collaboration.

3. **Open networked participation**, as a set of underlying practices that frame scholars' and other inolved parties' uses of digital technologies and that are informed by certain grounding assumptions and values regarding accessibility, democratization, justice and the assertion of fundamental human rights via the spread of equitable knowledge along its life-cycle of creation, dissemination and re-use along. Open networked participation is a process happening under constant negotiation of one's and others' personal, institutional, legal, and other social factors and resulting boundaries.

*This definition is an adaption and remix of earlier definitory elements taken from Veletsianos and Kimmons (2012), Weller (2014), and DeRosa and Jhangiani (2018).*

#### References

Veletsianos, George and Royce Kimmons. (2012). Assumptions and Challenges of Open Scholarship. The International Review of Research in Open and Distributed Learning, vol. 13 (4), 166-89. doi: [10.19173/irrodl.v13i4.1313](https://doi.org/10.19173/irrodl.v13i4.1313).

Weller, Martin. (2014). The Battle For Open: How openness won and why it doesn’t feel like victory. doi: [10.5334/bam](https://doi.org/10.5334/bam).

DeRosa, Robin and Rajiv Jhangiani. (2018). What is Open Pedagogy? [openpedagogy.org](http://openpedagogy.org/open-pedagogy/).

---