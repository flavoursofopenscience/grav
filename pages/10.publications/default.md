---
title: Publications
hide_hypothesis: false
sitemap: {  }
external_links:
    process: true
    title: true
    no_follow: false
    target: _blank
    mode: active
twitterenable: true
twittercardoptions: summary
articleenabled: false
article: {  }
personenabled: false
facebookenable: true
primaryImage:
    pawel-czerwinski-BAiRfbt1HRE-unsplash-cropped.jpg:
        name: pawel-czerwinski-BAiRfbt1HRE-unsplash-cropped.jpg
        type: image/jpeg
        size: 737952
        path: pawel-czerwinski-BAiRfbt1HRE-unsplash-cropped.jpg
published: true
date: '2021-06-19 19:47'
media_order: 'pawel-czerwinski-BAiRfbt1HRE-unsplash-cropped.jpg,publications-tobias-steiner-rdf.rdf,steiner-2012-dealing-with-a-nations-trauma.pdf'
---

_Please note that this list of publications is also available on_ [Zotero](https://www.zotero.org/flavorsofopenscience)_, thank you_.

===

## Blog posts

### 2021

Steiner, Tobias, and Lucy Barnes. ‘COPIM Newsflash! \#3 Project Update May 2021’. *COPIM*, 9 June 2021. <https://doi.org/10.21428/785a6451.7400a855>.

Schwamm, Hardy, and Tobias Steiner. ‘Open Voices: Interview with Toby Steiner’. *Open Voices* (blog), 26 March 2021. <https://hardimanlibrary.blogspot.com/2021/03/open-voices-interview-with-toby-steiner.html>.

Adema, Janneke, and Tobias Steiner. ‘New COPIM WP6 Report Released Today: “Books Contain Multitudes: Exploring Experimental Publishing”’. *COPIM Open Documentation Site* (blog), 1 February 2021. <https://copim.pubpub.org/pub/new-copim-wp6-report-released-today-books-contain-multitudes-exploring-experimental-publishing/>.

### 2020

Steiner, Tobias. ‘Using PubPub for Scholarly Output: Import, Citations, Collaboration, and Zotero’. *COPIM Open Documentation Site* (blog), 17 July 2020. <http://doi.org/10.21428/785a6451.f72bab42>.

Barnes, Lucy, Dan Rudmann, and Tobias Steiner. ‘COPIM Newsflash! \#2 Project Update June 2020’. *COPIM Open Documentation Site* (blog), 22 June 2020. <https://copim.pubpub.org/pub/copim-newsflash-2-june-2020/>.

### 2019

Steiner, Tobias. ‘OpenAIRE and EOSC-Hub @ EOSC Symposium 2019’. OpenAIRE (blog), 6 December 2019. https://www.openaire.eu/blogs/openaire-and-eosc-hub-at-eosc-symposium-2019.

Steiner, Tobias. ‘OpenAIRE and EOSC-Hub: Collaboration Is Key’. OpenAIRE (blog), 4 November 2019. https://www.openaire.eu/blogs/openaire-and-eosc-hub-collaboration-is-key.

Steiner, Tobias. ‘TV Studies for All? On Open Access and Publishing in TV and Media Studies’. *CSTonline*, 22 February 2019. <http://doi.org/10.17613/B13H-J967>.

### 2018 and older

Steiner, Tobias. ‘Subversion of Nostalgia as a Strategy of Engagement in Alternate History TV: 11.22.63 and The Man in the High Castle’. *CSTonline*, 1 June 2018. <https://doi.org/10.17613/M69882M32>.

Steiner, Tobias. ‘“Have You Ever Tried to Un-Make Soup?” Legion’s Roller-Coaster Ride through the Sixties’. *CSTonline*, 25 April 2017. <https://doi.org/10.17613/M6XF68>.

Steiner, Tobias. ‘Meticulous World-Building in Space: The Expanse, and the Current Resurgence of Science Fiction on TV’. *CSTonline*, 17 March 2016. <https://doi.org/10.17613/m6249j>.

## Articles & contributions in edited collections (peer-reviewed)

### 2021

Steiner, Tobias. ‘Open Knowledge Infrastructures in Times of the Pandemic:  Lessons from the first year of COPIM’. *Commonplace*, 26 March 2021. <https://doi.org/10.21428/6ffd8432.44d61468>.

Steiner, Tobias. ‘Bron/Broen, the Pilot as Space between Cultures, and (Re)Negotiations of Nordic Noir’. In *The Scandinavian Invasion: The Nordic Noir Phenomenon and Beyond*. eds. McCulloch, Richard, and William Proctor. Peter Lang, 2021 (in print). Preprint: <https://doi.org/10.17605/osf.io/ug9x7>.

### 2020

Tennant, Jonathan P., Paola Masuzzo, Tobias Steiner, Bastian Greshake Tzovaras, and Natalia Z. Bielczyk. ‘Introducing Massively Open Online Papers (MOOPs)’. *KULA: Knowledge Creation, Dissemination, and Preservation Studies*, 20 April 2020. <https://doi.org/10.5334/kula.63>.

### 2019 and older

Steiner, Tobias. ‘What Would Jack Bauer Do? Negotiating Trauma, Vengeance and Justice in the Cultural Forum of Post-9/11 TV Drama, from 24 to Battlestar Galactica and Person of Interest’. *European Journal of American Studies* 13, no. 4 (17 March 2019). <https://doi.org/10.4000/ejas.14045>.

Steiner, Tobias. ‘Dirigir El Discurso: La Construcción de La Autoría En La Televisión de Calidad, y El Caso de Juego de Tronos’. Translated by Miguel Bustos García. *Estudios Cinematográficos* 1 (1 April 2018): 67–82. <https://doi.org/10.22201/cuec.01888056p.2018.1.35>.

Steiner, Tobias. ‘Metadaten Und OER : Geschichte Einer Beziehung’. *Synergie: Fachmagazin Für Digitalisierung in Der Lehre* 4 (2017). <https://doi.org/10.17613/M6P81G>.

Steiner, Tobias. ‘OpenLab. Nexus Der Entwicklung in Richtung Openness’. *Synergie: Fachmagazin Für Digitalisierung in Der Lehre* 4 (2017). <https://doi.org/10.17613/M6T24S>.

Steiner, Tobias. ‘Openness Vor Ort’. *Synergie: Fachmagazin Für Digitalisierung in Der Lehre* 3 (2017). <https://doi.org/10.17613/m6qf4n>.

Steiner, Tobias. ‘SynLLOER - Offene Bildungsmaterialien in breite Wahrnehmung bringen’. *Synergie: Fachmagazin Für Digitalisierung in Der Lehre* 2 (2016): 37–38. <https://d-nb.info/1120622239/34>.

Steiner, Tobias. ‘Nachhaltigkeit Und Vernetzung als Erfolgsfaktoren bei der Implementierung digitalen Lehrens und Lernens’. In *Die Offene Hochschule: Vernetztes Lehren Und Lernen*, eds. Nicolas Apostolopoulos, Wolfgang Coy, Karoline von Köckritz, Ulrike Mußmann, Heike Schaumburg, and Andreas Schwill, 99–110. Grundfragen Multimedialen Lehrens Und Lernens. Münster: Waxmann, 2016. <https://refubium.fu-berlin.de/bitstream/handle/fub188/21851/GML-2016_Tagungsband.pdf>.

Mayrberger, Kerstin, and Tobias Steiner. ‘interdisziplinär, integriert & vernetzt - Organisations- und Lehrentwicklung mit digitalen Medien heute’. In *Digitale Medien und Interdisziplinarität: Herausforderungen, Erfahrungen, Perspektiven*, 68:13–23. Medien in der Wissenschaft. Münster: Waxmann, 2015. <https://www.pedocs.de/volltexte/2015/11322/pdf/Mayrberger_Steiner_2015_interdisziplinaer_integriert_vernetzt.pdf>.

Armbrust, Sebastian, and Tobias Steiner. ‘Review: Jonas Nesselhauf / Markus Schleich (Hrsg.) (2014): Quality-TV. Die Narrative Spielwiese Des 21. Jahrhunderts⁈ Münster: LIT’. *Medien & Kommunikationswissenschaft* 63, no. 2 (2015): 293–94. <https://doi.org/10.5771/1615-634x-2015-2-293>.

Steiner, Tobias. ‘Steering the Author Discourse: The Construction of Authorship in Quality TV, and the Case of Game of Thrones’. *SERIES - International Journal of TV Serial Narratives* 1, no. 2 (2015): 181–92. <https://doi.org/10.6092/issn.2421-454x/5903>.

Steiner, Tobias. ‘Under the Macroscope: Convergence in the US Television Market between 2000 and 2014’. *IMAGE: Journal of Interdisciplinary Image Sciences* 22, no. 1 (2015): 4–21. <https://doi.org/10.17613/M6KR2R>.

Steiner, Tobias. ‘Transmediales Erzählen Im Narrativen Universum von “Game of Thrones”’. *Journal of Serial Narration on Television* 4 (2013): 53–61. <https://doi.org/10.17613/M69C2G>.

## Preprints & position papers

### 2020

Tennant, Jonathan, Tobias Steiner, and Neo Chung. ‘Major Socio-Cultural Barriers to Widespread Adoption of Open Scholarship’, 6 April 2020. <https://doi.org/10.31235/osf.io/bth73>.

Tennant, Jonathan, Benedikt Fecher, Wojciech Francuzik, Tobias Steiner, Daniel Dunleavy, and Monica Gonzalez-Marquez. ‘Open Scholarship as a Mechanism for the United Nations Sustainable Development Goals’, 24 March 2020. <https://doi.org/10.31235/osf.io/8yk62>.

Tennant, Jonathan, Ritwik Agarwal, Ksenija Baždarić, David Brassard, Tom Crick, Daniel J. Dunleavy, Thomas Rhys Evans, et al. ‘A Tale of Two “Opens”: Intersections between Free and Open Source Software and Open Scholarship’, 5 March 2020. <https://doi.org/10.31235/osf.io/2kxq8>.

### 2019

Steiner, Tobias. ‘What Do We Talk about When We Talk about “Open”? On Education, Science, Research, and Open Scholarship’. Frankfurt: Leibniz Research Alliance Open Science, 2019. <https://doi.org/10.5281/zenodo.3540839>.

Steiner, Tobias. *Teaching Practices & Values of Open Science and Scholarship Needs Open Infrastructure!*, 2019. <https://doi.org/10.31235/osf.io/aksxq>.

Tennant, Jonathan P., Bruce Becker, Tanja de Bie, Julien Colomb, Valentina Goglio, Ivo Grigorov, Chris H. J. Hartgerink, et al. ‘What Collaboration Means to Us: We Are More Powerful When We Work Together as a Community to Solve Problems’. *Collaborative Librarianship* 11, no. 2 (26 July 2019). <https://digitalcommons.du.edu/collaborativelibrarianship/vol11/iss2/2/>.

Tennant, Jonathan, Jonathan Dugan, Rachel Harding, Tony Ross-Hellauer, Kshitiz Khanal, Thomas Pasquier, Jeroen Bosman, Tobias Steiner, et al. ‘Open-Scholarship-Strategy/Indexed: Foundations for Open Scholarship Strategy Development: Second Formal Release’, 5 February 2019. <http://doi.org/10.5281/zenodo.1323436>.

Tennant, Jonathan P., Jennifer Beamer, Jeroen Bosman, Björn Brembs, Neo Christopher Chung, Gail Clement, Tom Crick, Tobias Steiner, et al. ‘Foundations for Open Scholarship Strategy Development’, 30 January 2019. <https://doi.org/10.31222/osf.io/b4v8p>.

## Reports & Whitepapers

### 2021

Adema, Janneke, and Tobias Steiner. ‘New COPIM WP6 Report Released Today: “Books Contain Multitudes: Exploring Experimental Publishing”’. *COPIM Open Documentation Site* (blog), 1 February 2021. <https://copim.pubpub.org/pub/new-copim-wp6-report-released-today-books-contain-multitudes-exploring-experimental-publishing/>.

Adema, Janneke, Marcell Mars, and Tobias Steiner. *Books Contain Multitudes: Exploring Experimental Publishing*. COPIM, 2021. <https://doi.org/10.21428/785a6451.933fa904>.

Adema, Janneke, and Tobias Steiner. ‘Part 2: A Typology of Experimental Books’. In *Books Contain Multitudes: Exploring Experimental Publishing*. COPIM, 2021. <https://doi.org/10.21428/785a6451.cd58a48e>.

Mars, Marcell, Tobias Steiner, and Janneke Adema. ‘Part 3: Technical Workflows and Tools for Experimental Publishing’. In *Books Contain Multitudes: Exploring Experimental Publishing*. COPIM, 2021. <https://doi.org/10.21428/785a6451.174760b2>.

### 2020

Steiner, Tobias, and Janneke Adema. ‘Community-Led Open Publication Infrastructures for Monographs (COPIM): Annual Report - Year 1 (2019-2020)’, 20 October 2020. <https://doi.org/10.5281/zenodo.4107104>.

Stone, Graham, Rupert Gatti, Vincent W. J. van Gerven Oei, Javier Arias, Tobias Steiner, and Eelco Ferwerda. ‘WP5 Scoping Report: Building an Open Dissemination System’. COPIM, 17 July 2020. <https://doi.org/10.5281/zenodo.3961564>.

Steiner, Tobias, and Najla Rettberg. ‘D3.3 - Overview of Progress on Topical Task Forces’. OpenAIRE, 13 March 2020. <https://doi.org/10.5281/zenodo.4148744>.

Aasheim, Jens H, Jochen Schirrwagen, Iryna Kuchma, Gwen Franck, Emilie Hermans, Najla Rettberg, and Tobias Steiner. ‘D6.2 – Best Practice Guide for Co-Operative Models of Publishing’. OpenAIRE, 31 January 2020. <https://doi.org/10.5281/zenodo.3701427>.

### 2019

Manola, Natalia, Najla Rettberg, Paolo Manghi, Mike Mertens, Birgit Schmidt, Tobias Steiner, Prodromos Tsiavos, Eloi Rodrigues, Nina Karlstrom, and Mojca Kotar. ‘Achieving Open Science in the European Open Science Cloud. Setting out OpenAIRE’s Vision and Contribution to EOSC’. OpenAIRE, 27 September 2019. <https://doi.org/10.5281/zenodo.3610132>.

Manola, Natalia, Najla Rettberg, Paolo Manghi, Mike Mertens, Birgit Schmidt, Tobias Steiner, Prodromos Tsiavos, Eloy Rodrigues, Nina Karlstrom, and Mojca Kotar. ‘Achieving Open Science in the European Open Science Cloud. Setting out OpenAIRE’s Vision and Contribution to EOSC - Executive Summary’. OpenAIRE, 26 September 2019. <https://doi.org/10.5281/zenodo.3557390>.

Slobodeaniuk, Markus, Tobias Steiner, Nina Rüttgens, and Martin Lohse. *Angebot ≠ Auftrag. Aktivitäten Im Universitätskolleg Digital 2017/2018*. Edited by Kerstin Mayrberger. Synergie Sonderband. Hamburg: Universitätskolleg der Universität Hamburg, 2019. <https://doi.org/10.25592/978.3.924330.70.5>.

### 2010-2018

Slobodeaniuk, Markus, Tobias Steiner, and Benjamin Gildemeister. *Webtools des Teilprojekts 43. Etablierung von LimeSurvey, Piwik und Yourls*. Prozessdokumentation Universitätskolleg, Band 04. Hamburg: Universitätskolleg der Universität Hamburg, 2016. <https://epub.sub.uni-hamburg.de/epub/volltexte/2017/62226/pdf/prozdok_band4_web.pdf>.

Steiner, Tobias. ‘Grundlagen Für Die Entwicklung Einer Open Scholarship-Strategie’, 17 February 2019. <https://doi.org/10.31235/osf.io/hu9sj>.

Mayrberger, Kerstin, Tobias Steiner, Manfred Steger, and Markus Slobodeaniuk, *Projekte Der BMBF-Förderung OERInfo 2017/2018*. Synergie Sonderband. Hamburg: Universitätskolleg der Universität Hamburg, 2018. <https://doi.org/10.25592/978.3.924330.64.4>.

Steiner, Tobias, Benjamin Gildemeister, and Nicolai Krolzik. *Online-Self-Assesments*. Ed. Ulrike Helbig. Prozessdokumentation Universitätskolleg, Band 02. Hamburg: Universitätskolleg der Universität Hamburg, 2015. <https://www.universitaetskolleg.uni-hamburg.de/media/prozdok/uk-prozdok-band2-selfassessments-online.pdf>.

Steiner, Tobias, Nina Rüttgens, Martin Lohse, and Vassilios Gourdomichalis. *Projekte 2017/2018. Berichte Aus Der Förderphase Am Universitätskolleg Digital*. Ed. Kerstin Mayrberger. Synergie Sonderband. Hamburg: Universitätskolleg der Universität Hamburg, 2019. <https://doi.org/10.25592/978.3.924330.73.6>.

## Misc. (code, infographics, translations, intro material)

Tennant, Jon, Rachael Ainsworth, Tobias Steiner, Lyndie Ruth Chiou, George MacGregor, Ritwik Agarwal, and Encarnación Martínez Álvarez. *OpenScienceMOOC/Module-6-Open-Access-to-Research-Papers: First Pre-Release*. *Zenodo*, 2019. <https://doi.org/10.5281/zenodo.3358852>.

Tennant, Jonathan, Jonathan Dugan, Rachel Harding, Tony Ross-Hellauer, Kshitiz Khanal, Thomas Pasquier, Jeroen Bosman, et al. ‘Open-Scholarship-Strategy/Indexed: Foundations for Open Scholarship Strategy Development: Second Formal Release’, 5 February 2019. <http://doi.org/10.5281/zenodo.1323436>.

Tennant, Jonathan P., Jennifer Beamer, Jeroen Bosman, Björn Brembs, Neo Christopher Chung, Gail Clement, Tom Crick, et al. ‘Foundations for Open Scholarship Strategy Development’, 30 January 2019. <https://doi.org/10.31222/osf.io/b4v8p>.

Steiner, Tobias. ‘Open Scholarship’. Queios, 20 August 2019. <https://doi.org/10.32388/282729>.

Steiner, Tobias, Manfred Steger, Lucas Jacobsen, and Sophia Zicari. *OER-Know-How 2018*. *Universität Hamburg*. Synergie Praxis. Hamburg: Universitätskolleg der Universität Hamburg, 2018. <https://doi.org/10.25592/issn2513-2059.002>.

Jonas, Ulrich, Christina Schwalbe, and Tobias Steiner. *Open Educational Resources (OER) 2017*. Synergie Praxis. Hamburg: Universitätskolleg der Universität Hamburg, 2017. <https://d-nb.info/113864188X/34>.

Steiner, Tobias. *Offene Bildungspraxis: Mögliche Szenarien*. 20 February 2018. <https://doi.org/10.5281/zenodo.1181884>.

Steiner, Tobias. *Open Educational Practice (OEP): Collection of Scenarios*. 23 February 2018. <https://doi.org/10.5281/zenodo.1183806>.

Steiner, Tobias. *Das Wordpress-Kompendium*. Band 06. Hamburg: Universitätskolleg der Universität Hamburg, 2016. <https://d-nb.info/1116746174/34>.

Winston, Brian. *„Ça va de Soi“. Die Visuelle Repräsentation von Gewalt Im Holocaust-Dokumentarfilm*. Translated by Tobias Steiner. *AVINUS Verlag*. Vol. 10/2015. Hefte Zur Medienkulturforschung. Berlin: AVINUS Verlag, 2015. <https://doi.org/10.17613/m6nn8m>.

Steiner, Tobias. ‘TV or Not TV? Historical Development of US American Serial Television from the 1950s up to Today // Historische Entwicklung US-Amerikanischer Fernsehserien von 1950 Bis Heute’, 1 October 2013. Syllabus. <https://doi.org/10.17613/M62V71>.

Steiner, Tobias. ‘Dealing with a Nation’s Trauma: Allegories on 9/11 in Contemporary Serial US Television Drama Narratives and the Case of Homeland’. MA Thesis, Birkbeck, University of London, 2012. <https://flavoursofopen.science/user/pages/10.publications/steiner-2012-dealing-with-a-nations-trauma.pdf>

Steiner, Tobias. ‘“FlashForward”: An Experiment in Collective Memory Studies’. BA Thesis, Universität Hamburg, 2011. <https://doi.org/10.17613/M6PB49>.

 

    
---

Header photo by <a href="https://unsplash.com/photos/se0HlB8rbjw">Ekrulila</a> on Unsplash.  
