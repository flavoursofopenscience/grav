---
title: 'Timeline: Open Source, Open Access & Open Scholarship'
published: true
date: '2019-05-12 15:50'
publish_date: '2019-05-12 15:50'
hide_hypothesis: false
external_links:
    target: _blank
    mode: active
googledesc: 'H5P timeline "Open Source, Open Access & Open Scholarship" by Tobias Steiner is licensed under a Creative Commons Attribution 4.0 International License.'
twitterenable: true
twittercardoptions: summary_large_image
twitterdescription: 'H5P timeline "Milestones in Open Source, Open Access & Open Scholarship" by Tobias Steiner is licensed under a Creative Commons Attribution 4.0 International License.'
articleenabled: false
personenabled: false
facebookenable: true
facebookdesc: 'H5P presentation "Timeline: Open Source, Open Access & Open Scholarship" by Tobias Steiner is licensed under a Creative Commons Attribution 4.0 International License.'
fontawesome: use_global
animate_css: use_global
wow_js: use_global
google_prettify: use_global
foundation_abide_js: use_global
foundation_accordion_js: use_global
foundation_accordionMenu_js: use_global
foundation_drilldown_js: use_global
foundation_dropdown_js: use_global
foundation_dropdownMenu_js: use_global
foundation_equalizer_js: use_global
foundation_interchange_js: use_global
foundation_magellan_js: use_global
foundation_offcanvas_js: use_global
foundation_orbit_js: use_global
foundation_responsiveMenu_js: use_global
foundation_responsiveToggle_js: use_global
foundation_reveal_js: use_global
foundation_slider_js: use_global
foundation_sticky_js: use_global
foundation_tabs_js: use_global
foundation_toggler_js: use_global
foundation_tooltip_js: use_global
foundation_util_box_js: use_global
foundation_util_keyboard_js: use_global
foundation_util_mediaQuery_js: use_global
foundation_util_motion_js: use_global
foundation_util_nest_js: use_global
foundation_util_timerAndImageLoader_js: use_global
foundation_util_touch_js: use_global
foundation_util_triggers_js: use_global
textsize:
    scale: ''
    modifier: '1'
style:
    header-font-family: ''
    header-color: ''
    block-font-family: ''
    block-color: ''
    background-color: ''
    background-image: ''
    background-size: ''
    background-repeat: ''
    justify-content: ''
    align-items: ''
feed:
    limit: '10'
    description: ''
class: ''
footer: ''
presentation:
    content: ''
    parser: ''
    styles: ''
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: '5'
    pagination: '1'
    url_taxonomy_filters: '1'
primaryImage: {  }
twittertitle: 'Timeline: Open Source, Open Access & Open Scholarship | flavoursofopen'
---

Updated for Open Access Week 2021.

<iframe src="https://blog.flavoursofopen.science/wp-admin/admin-ajax.php?action=h5p_embed&id=1" allowfullscreen="allowfullscreen" style="margin: 0px auto; display: block; width: 75%;" width="1535" height="626" frameborder="0"></iframe></code>

<p style="font-size:0.75em;"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png"/></a>
 <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">H5P timeline "Open Source, Open Access & Open Scholarship"</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://blog.flavoursofopen.science/timeline" property="cc:attributionName" rel="cc:attributionURL">Tobias Steiner</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</p>

This timeline has first been developed as a supporting document to our collaborative paper (featured on page 6):

> Tennant, Jonathan, Ritwik Agarwal, Ksenija Baždarić, David Brassard, Tom Crick, Daniel J. Dunleavy, Thomas R. Evans, et al. 2020. “A Tale of Two 'opens': Intersections Between Free and Open Source Software and Open Scholarship.” SocArXiv. March 6. <a href="https://doi.org/10.31235/osf.io/2kxq8">https://doi.org/10.31235/osf.io/2kxq8</a>

      