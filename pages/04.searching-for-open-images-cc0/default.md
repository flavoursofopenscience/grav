---
title: 'Looking for pubic domain images to use for Open Content? (2019 edition)'
media_order: clem-onojeghuo-QBvtgLdmTbQ-unsplash.jpg
published: true
date: '2019-07-06 18:47'
metadata:
    Keywords: 'public domain'
hide_hypothesis: false
external_links:
    process: true
    target: _blank
    mode: active
fontawesome: use_global
animate_css: use_global
wow_js: use_global
google_prettify: use_global
foundation_abide_js: use_global
foundation_accordion_js: use_global
foundation_accordionMenu_js: use_global
foundation_drilldown_js: use_global
foundation_dropdown_js: use_global
foundation_dropdownMenu_js: use_global
foundation_equalizer_js: use_global
foundation_interchange_js: use_global
foundation_magellan_js: use_global
foundation_offcanvas_js: use_global
foundation_orbit_js: use_global
foundation_responsiveMenu_js: use_global
foundation_responsiveToggle_js: use_global
foundation_reveal_js: use_global
foundation_slider_js: use_global
foundation_sticky_js: use_global
foundation_tabs_js: use_global
foundation_toggler_js: use_global
foundation_tooltip_js: use_global
foundation_util_box_js: use_global
foundation_util_keyboard_js: use_global
foundation_util_mediaQuery_js: use_global
foundation_util_motion_js: use_global
foundation_util_nest_js: use_global
foundation_util_timerAndImageLoader_js: use_global
foundation_util_touch_js: use_global
foundation_util_triggers_js: use_global
googletitle: ' Looking for pubic domain images to use for Open Content? (2019 edition) '
googledesc: 'Where to find open, CC0 public-domain pictures for easy re-use in OER and other content without customized Unsplash and Pixabay licenses'
twitterenable: true
twittercardoptions: summary
twittershareimg: /searching-for-open-images-cc0/clem-onojeghuo-QBvtgLdmTbQ-unsplash.jpg
twittertitle: ' Tobias Steiner Administrator      Dashboard     Configuration     Pages 7     Plugins 31     Themes 12     Tools     Logout  Copy Move Delete Looking for pubic domain images to use for Open Content? (2019 edition) '
twitterdescription: 'Where to find open, CC0 / CC-Zero public-domain pictures for easy re-use in OER and other content.'
articleenabled: false
personenabled: false
facebookenable: true
primaryImage: {  }
---

![](clem-onojeghuo-QBvtgLdmTbQ-unsplash.jpg)



The year 2019 has not been kind to the Open Content community, particularly for those wanting to use CC0-/public domain-licensed images for remixing in presentations, websites, etc. 

Many of the major platforms that have grown a reputation of making open, CC0-licensed images available for everyone decided to take a step back and introduce tailored licenses that would explicitly forbit the large-scale copying of content from their respective servers (a strategy that has led to the establishmend of "copycat platforms" such as Pexels, which mainly served content that had been copied from e.g. Pixabay and Gratisography, and which now has also introduced [its own license](https://www.pexels.com/photo-license/)).

Now you might ask what's the fuzz about this? 

All in all, it's about clarity - with universal CC0 attribution, people knew what they were using, and image creators had a way of making sure that re-use was allowed no matter what - making an image available via CC0 meant that the author/copyright holder agrees with others using this work in any way, be it commercially or not, and allowing alterations without the need of attribution (altough many of course appreciate it ;) ). 

With the rise of platform-specific licensing, this universal intention has now been subverted - see e.g. [irights.info](https://irights.info/artikel/pixabay-wechselt-von-creative-commons-freigabe-zu-selbstgestrickter-lizenz/29410) for a detailed discussion of Pixabay's choice of a new license (in German)). For programmers, and with them theme developers in particular, this has further implications, as [pixelrockstar](https://www.pixelrockstar.com/pixabay-joins-unsplash-image-licensing/) notes, quoting Carolina Nymark: "Long story short, under the new Pixabay License the photos will not be GPL compatible".

But fear not, there are still a variety of options that can help you find image material that is licensed under Creative Commons Zero, thus allowing re-use for any open content whatsoever.

### First stop

* [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) - features CC0 & public domain as well as other variations of open-licensed images
* [Flickr Commons](https://www.flickr.com/commons/) - public domain

### Here's a list of platforms that feature **CC0-licensed** public domain images in 2019

* [stocksnap.io](https://stocksnap.io/) - photos
* [freestocks.org](https://freestocks.org/) - photos
* [plixs](https://plixs.com/) - photos
* [picography.co](https://picography.co/) - photos
* [mmtstock](https://mmtstock.com/) - photos, videos & templates
* [skitterphoto](https://skitterphoto.com/) - photos
* [New Old Photos](https://nos.twnsnd.co/) - curated collections sourced from Flickr Commons
* [Jay Mantri](https://jaymantri.com/) - seven new pics each Thursday, all CC0


#### special cases

* [pikwizard](https://pikwizard.com/) - although the platform itself doesn't openly advertise CC0 images, many of the images on offer are then licensed as CC0
* [RawPixel](https://www.rawpixel.com/category/53/public-domain) - not all images on the platform are public domain, but a distinct category allows browsing through the PD offers they have

#### Free, but **non-CC0 / non-Public Domain** platforms

still [the usual](https://learn.g2.com/free-stock-photos), including... 

* [Unsplash](http://unsplash.com/)
* [Pexels](https://www.pexels.com/)
* [Pixabay](https://pixabay.com/)
* [Kaboompics](https://kaboompics.com/)
* [Burst](https://burst.shopify.com/)
* [Reshot](https://www.reshot.com/)
* [Gratisography](https://gratisography.com/)
* [freepik](https://www.freepik.com) - vector images
* [vecteezy](https://www.vecteezy.com/) - vector images
* [flaticon](https://www.flaticon.com/) - vector icons
* [freevectors](https://www.freevectors.net/) - vector images
* etc.

---

EDIT: 2020-01-17: There's a more detailed list that explores the CC0-ability of platforms and search engines serving images, icons, vector graphics etc., thankfully released  by Johannes Schirge ([@jschirge](https://twitter.com/jschirge/status/1432331106364403714) on Twitter) under CC0.

▶ find it here on his "Website with 'free' pictures" [GoogleDoc](https://docs.google.com/document/d/14ahmnEl8IJCpVy-ilPOn__kiMhUAKPXZujHUalgULWQ/view)


---

`Header image:` [Clem Onojeghuo](https://unsplash.com/photos/QBvtgLdmTbQ) `on Unsplash`