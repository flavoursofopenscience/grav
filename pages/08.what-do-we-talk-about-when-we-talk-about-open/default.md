---
title: 'What do we talk about when we talk about “Open”? On Education, Science, Research, and Open Scholarship'
date: '15:21 17-11-2019'
metadata:
    Keywords: 'Open Education, Open Science, Open ecosystem, entanglement, Open Scholarship'
hide_hypothesis: false
googledesc: 'Abstract & slides of the ignition talk at the Leibniz Research Alliance OpenScience symposium "Open Practices IN Education (OPINE)"'
twitterenable: true
twittercardoptions: summary_large_image
twittershareimg: /what-do-we-talk-about-when-we-talk-about-open/Capture.JPG
twittertitle: ' What do we talk about when we talk about “Open”? On Education, Science, Research, and Open Scholarship '
twitterdescription: 'Abstract & slides of the ignition talk at the Leibniz Research Alliance OpenScience symposium "Open Practices IN Education (OPINE)", 14.-15. November 2019, Frankfurt/Main.'
articleenabled: true
article:
    author: 'Tobias Steiner'
    datePublished: '13:59 13-11-2019'
    image_url: /what-do-we-talk-about-when-we-talk-about-open/Capture.JPG
personenabled: true
addperson:
    -
        person_name: 'Tobias Steiner'
        person_jobTitle: null
        person_address_addressLocality: null
        person_address_addressRegion: null
facebookenable: true
facebooktitle: ' What do we talk about when we talk about “Open”? On Education, Science, Research, and Open Scholarship '
facebookdesc: 'Abstract & slides of the ignition talk at the Leibniz Research Alliance OpenScience symposium "Open Practices IN Education (OPINE)", 14.-15. November 2019, Frankfurt/Main.'
facebookimg: /what-do-we-talk-about-when-we-talk-about-open/Capture.JPG
primaryImage:
    user/pages/08.what-do-we-talk-about-when-we-talk-about-open/patrick-mcmanaman-LN_g3qA8ohg-unsplash.jpg:
        name: patrick-mcmanaman-LN_g3qA8ohg-unsplash.jpg
        type: image/jpeg
        size: 1801953
        path: user/pages/08.what-do-we-talk-about-when-we-talk-about-open/patrick-mcmanaman-LN_g3qA8ohg-unsplash.jpg
fontawesome: use_global
animate_css: use_global
wow_js: use_global
google_prettify: use_global
foundation_abide_js: use_global
foundation_accordion_js: use_global
foundation_accordionMenu_js: use_global
foundation_drilldown_js: use_global
foundation_dropdown_js: use_global
foundation_dropdownMenu_js: use_global
foundation_equalizer_js: use_global
foundation_interchange_js: use_global
foundation_magellan_js: use_global
foundation_offcanvas_js: use_global
foundation_orbit_js: use_global
foundation_responsiveMenu_js: use_global
foundation_responsiveToggle_js: use_global
foundation_reveal_js: use_global
foundation_slider_js: use_global
foundation_sticky_js: use_global
foundation_tabs_js: use_global
foundation_toggler_js: use_global
foundation_tooltip_js: use_global
foundation_util_box_js: use_global
foundation_util_keyboard_js: use_global
foundation_util_mediaQuery_js: use_global
foundation_util_motion_js: use_global
foundation_util_nest_js: use_global
foundation_util_timerAndImageLoader_js: use_global
foundation_util_touch_js: use_global
foundation_util_triggers_js: use_global
---

_Ignition talk at the Leibniz Research Alliance OpenScience research symposium "Open Practices IN Education (OPINE)", November 14-15, 2019, Frankfurt/Main (Germany)._

##### Slides: 

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ6l2-Z4emZilbGE52ve51SHRMZF3JYQuN1T0uQcHSdE6-f1ue4vb6hOzeTMxKZnAogVIWwIw67xnpU/embed?start=false&loop=false&delayms=5000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

##### Links: 

* symposium abstracts available online on [Zenodo](https://zenodo.org/communities/opine/)
* extended bibliography, see the open library at [Zotero](https://www.zotero.org/groups/2346073/open_research_open_science_open_scholarship/items/collectionKey/YKXN272U)

##### Abstract 


> An excellent candidate for sloganizing is the word ‘open’. Immediately one uses it, the options polarize. To be open … is to be not closed, restricted, prejudiced or clogged; but free, candid, generous, above board, mentally flexible, future-oriented, etc. The opposite [sic] does not bear thinking about, and there can be no third alternative. ‘Open’ is yum. (Hill, 1975)

Today, and more than ever, “Open” is *en vogue* and *yum!* – a generously-applied denominator used as a modifier of existing paradigms both outside and within academia to signal something, new, improved, and better than the status quo – or is it?

With a focus on the scholarly realm, the variety of Open ‘fields’ such as Open Data, Open Access, Open Source and Open Education are usually seen as a response to the still-prevailing neoliberalism governing higher education (see e.g. Kansa, 2014; Lawson, 2019), and corresponding privatization and lock-in of knowledge in all its forms by corporate stakeholders – a response that for many promise the realization of a Knowledge Commons (Benkler, 2006; Ferrari, Scardaci, & Andreozzi, 2018; Manola et al., 2019; Ostrom, 2006; Suber, 2007).

Hence, the promise of a convergence of open practices in research and, more rarely, education, that is implicit in approaches to Open Research, Open Science, and Open Scholarship is gaining prominence in scholarly discourse. For high-level stakeholders such as the European Commission, the “movement” of Open Science is even attributed the status of a “revolution” which has gained further momentum in the last years through massive [allocation of funding](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:52016DC0178&from=EN) for and subsequent work on Open Science-related projects in the [Horizon2020](https://ec.europa.eu/programmes/horizon2020/en/what-horizon-2020) funding line and beyond, with pan-European networks such as [OpenAIRE](https://ec.europa.eu/digital-single-market/en/blog/openaire-europes-hub-open-science), and the inception of a European Open Science Cloud (EOSC) pushing the Open Science agenda further.

Similarly, national research funders such as Research England (UK), the Wellcome Trust (UK), the German Research Foundation (DFG), and the Agence Nationale de la Recherche (ANR, France) are moving towards an integration of Open Access and Open Science principles as a prerequisite in their lines of funding. And national governments are increasingly acknowledging the transformative nature of Open Access, Open Science and Open Education by similarly including these aspects in their agendas (see e.g. [National Plan of Open
Science](https://cache.media.enseignementsup-recherche.gouv.fr/file/Recherche/50/1/SO_A4_2018_EN_01_leger_982501.pdf) in France).

Next to these top-down impulses, there exist a variety of grassroots movements comprising those working in academia who are lobbying towards a more systemic cultural change of the academic system towards a more inclusive, equitable and participatory understanding and practice of all academic fields of occupation, including science, research, education and scholarly communication.

As Peters and Roberts have pointed out, “the social processes and policies that foster openness [are understood] as an overriding educational and scientific value, evidenced in the growth of open source, open access, open education, and their convergences that characterize global knowledge communities” (Peters & Roberts, 2010, p. 4)

But still… while there are common denominators inherent in all of the Open movements, the sub-disciplines of Open are increasingly beginning to keep to their own echo chambers, slowly forgetting their underlying ideas, principles, and values – and all of this leads to an increasing fragmentation towards these sub-disciplines turning into “open silos” (Campbell, 2015). One major case in point might be that of the parallel, but mostly mutually ignored co-existence of Open Science and Open Education. Over the last two decades, both fields have developed into considerable global movements, but the synergetic benefits that could be achieved through an alignment of interests with respect to shared infrastructure and practices that are, in the end, addressing the same target group – the researcher/educator – are still to be tapped.

With my introductory talk at the “Open Practices in Education” research symposium, I want to extend an invitation to join me on a short journey through this fragmented Open landscape. Differentiating the variety of ways that “Open” is conceptualized in the sub-movements of Openness in academia, let us take a closer look at the histories informing each of the movements involved. Following this, we will then consider the entanglements and convergences that are existent to some extent in the larger ‘meta concepts’ of Open Science and Open Education, and their relationship to Open Scholarship. As a conclusion, we will further scrutinize the cross-sectoral threads and lines of inquiry that might help us understand to see in what ways these open silos might benefit from each other to bring about truly inclusive and equitable Open Scholarship that extends to all fields of activity in the life of those working in and with academia, hopefully making possible a true Knowledge Commons.

![](https://flavoursofopen.science/user/pages/08.what-do-we-talk-about-when-we-talk-about-open/Capture.JPG)

##### Keywords 

Open Education, Open Science, Open ecosystem, entanglement

### References

Atenas, J., Havemann, L., & Priego, E. (2015). Open Data as Open Educational Resources: Towards Transversal Skills and Global Citizenship. *Open Praxis*, *7*(4), 377–389. https://doi.org/10.5944/openpraxis.7.4.233

Bartling, S., & Friesike, S. (Eds.). (2014). *Opening Science: The Evolving Guide on How the Internet is Changing Research, Collaboration and Scholarly Publishing*. https://doi.org/10.1007/978-3-319-00026-8

Benkler, Y. (2006). *The wealth of networks: How social production transforms markets and freedom*. Yale University Press.

Budroni, P., & Hanslik, S. (2018, November 23). The Vienna Declaration on the European Open Science Cloud. Retrieved November 10, 2019, from https://eosc-launch.eu/declaration/

Campbell, L. (2015, June 8). Open Silos? Open data and OER. Retrieved November 10, 2019, from Open World website: https://lornamcampbell.wordpress.com/2015/06/08/open-silos-open-data-and-oer/

Chan, L., Okune, A., Hiller, R., Albornoz, D., & Posada, A. (Eds.). (2019). *Contextualizing openness: Situating open science*. https://ruor.uottawa.ca/handle/10393/39849

Corrall, S., & Pinfield, S. (2014). Coherence of Open Initiatives in Higher Education and Research: Framing a Policy Agenda. In M. Kindling & E. Greifeneder (Eds.), *IConference Berlin 2014*. https://doi.org/10.9776/14085

Cronin, C. (2018). *Openness and praxis: A situated study of academic staff meaning-making and decision-making with respect to openness and use of open educational practices in higher education* (NUI Galway). Retrieved from http://hdl.handle.net/10379/7276

Farrow, R. (2016). Constellations of openness. In M. Deimann & M. A. Peters (Eds.), *The philosophy of open learning*. New York: Peter Lang.

Ferrari, T., Scardaci, D., & Andreozzi, S. (2018). The Open Science Commons for the European Research Area. In P.-P. Mathieu & C. Aubrecht (Eds.), *Earth Observation Open Science and Innovation* (pp. 43–67). https://doi.org/10.1007/978-3-319-65633-5_3

Hill, B. V. (1975). What’s “open” about open education? In D. Nyberg (Ed.), The Philosophy of open education (pp. 3–13). London ; Boston: Routledge & K. Paul.

Jhangiani, R. S., & Biswas-Diener, R. (2017). *Open: The Philosophy and Practices that are Revolutionizing Education and Science*. https://doi.org/10.5334/bbc

Kansa, E. (2014, January 27). It’s the Neoliberalism, Stupid: Why instrumentalist arguments for Open Access, Open Data, and Open Science are not enough. Retrieved November 10, 2019, from Impact of Social Sciences: https://blogs.lse.ac.uk/impactofsocialsciences/2014/01/27/its-the-neoliberalism-stupid-kansa/

Kramer, B., & Bosman, J. (2018). Rainbow Of Open Science Practices. *Zenodo*. https://doi.org/10.5281/zenodo.1147024

Lawson, S. A. (2019). *Open Access Policy in the UK: From Neoliberalism to the Commons* (Birkbeck, University of London). Retrieved from http://stuartlawson.org/wp-content/uploads/2018/09/2018-09-03-Lawson-thesis.pdf

Le Plan national pour la science ouverte: Les résultats de la recherche scientifique ouverts à tous, sans entrave, sans délai, sans paiement. (n.d.). Retrieved November 10, 2019, from Ministère de l’Enseignement supérieur, de la Recherche et de l’Innovation website: https://www.enseignementsup-recherche.gouv.fr/cid132529/le-plan-national-pour-la-science-ouverte-les-resultats-de-la-recherche-scientifique-ouverts-a-tous-sans-entrave-sans-delai-sans-paiement.html

Manola, N., Rettberg, N., Manghi, P., Mertens, M., Schmidt, B., Steiner, T., … Kotar, M. (2019). *Achieving Open Science in the European Open Science Cloud: Setting out OpenAIRE’s vision and contribution to EOSC*. https://doi.org/10.5281/zenodo.3475076

McKiernan, E. C. (2017). Imagining the “open” university: Sharing scholarship to improve research and education. *PLOS Biology*, *15*(10), https://doi.org/10.1371/journal.pbio.1002614

Nerlich, B., Hartley, S., Raman, S., & Smith, A. (2018). *Science and the Politics of Openness: Here be monsters*. Manchester University Press.

OpenAIRE: Europe’s Hub for Open Science [Blog]. (2017, April 6). Retrieved November 10, 2019, from Digital Single Market—European Commission website: https://ec.europa.eu/digital-single-market/en/blog/openaire-europes-hub-open-science

Ostrom, E. (2006). *Understanding Knowledge as a Commons: From Theory to Practice* (C. Hess, Ed.). MIT Press.

Peters, M., & Roberts, P. (2010). *The virtues of openness: Education, science, and scholarship in the digital age*. London: Routledge.

Steiner, T. (2018). *Open Educational Practice (OEP): Collection of scenarios*. https://doi.org/10.5281/zenodo.1183806

Tennant, J., Beamer, J. E., Bosman, J., Brembs, B., Chung, N. C., Clement, G., … Turner, A. (n.d.). *Foundations for Open Scholarship Strategy Development*. https://doi.org/10.31222/osf.io/b4v8p

Veletsianos, G., & Kimmons, R. (2012). Assumptions and challenges of open scholarship. *The International Review of Research in Open and Distributed
Learning*, *13*(4), 166. https://doi.org/10.19173/irrodl.v13i4.1313

Weller, M. (2014). *The Battle For Open How openness won and why it doesn’t feel like victory*. https://doi.org/10.5334/bam

Weller, M. (2018). The Digital Scholar Revisited. *The Digital Scholar: Philosopher’s Lab*, *1*(2), 52–71. https://doi.org/10.5840/dspl20181218

Wiley, D., & Hilton III, J. (2009). Openness, Dynamic Specialization, and the Disaggregated Future of Higher Education. *The International Review of Research in Open and Distributed Learning*, *10*(5). https://doi.org/10.19173/irrodl.v10i5.768

