---
title: "Teaching practices & values of Open Science and Scholarship needs open infrastructure!"
author: "Tobias Steiner"
date: '2019-09-03'
layout: post
categories: community
---


# Teaching practices & values of Open Science and Scholarship needs open infrastructure!

by [Tobias Steiner][osmoocpeople]

<figure>
  ![A freeway interchange. Illustration.](https://gitlab.com/flavoursofopenscience/grav/raw/master/experimental/cars-2605953.jpg)
  <figcaption>
    <a href="https://pixabay.com/de/photos/2605953/">A freeway interchange</a>.
    Illustration by <a href="https://pixabay.com/de/users/StockSnap-894430/">StockSnap</a>.
  </figcaption>
</figure>

> Everything we have gained by opening content and data will be under threat if
> we allow the enclosure of scholarly infrastructures.

— Geoffrey Bilder, Jennifer Lin, Cameron Neylon (2015).

---

#### **tldr;**

> The need to empower scholars as well as the general public to practice Open
> Scholarship has been a recurring theme in public scholarly discourse of the
> last dacade and beyond. Still, the tools and platforms required to make this a
> reality are usually in the hands of - often US-based - for-profit companies.
> And while there exist a lot of specialized platforms out there, publicly-funded
> training, project management and scholarly communications platforms are close
> to non-existent. With this post, the OpenScienceMOOC team wants to open a
> discussion about how an open networked scholarly infrastructure for learning,
> teaching and scholarly communication for Open Scholarship could look like.

Open Science has been the talk of the town for quite some time now. And while there has been made amazing progress on a variety of levels, we still face certain blind spots when it comes to the provision of basic infrastructure that would allow us to truyl make knowledge an equitable public good independent from corporate influence and control.

While much of the current focus has been on how to develop alternative, open publication systems, or else how repositories can be made available to host all kinds of open data and content, the whole field of training and communications still remains generally dependent on tools and platforms that are under the influence of commercial, corporate influence.

Let me illustrate this dilemma: We at the [Open Science MOOC][opensciencemooc] are a non-profit, community-led project that exists out of the pure enthusiasm of its participating members. This also means that many of us are early career researchers (ECRs) who currently don't or will not in the foreseeable future have the privilege of long-term support by their home institution. This is often simply due to the fact that we are all part of academia's grind-down economy that has many of us working on short-term and part-time contracts that tend to be limited to periods usually ranging from three months to two years.

Now, with the evolution of the Open Science MOOC over the last two years towards a global network that features researchers from almost any country in the world (see our shiny new [Open Science MOOC dashboard][osmoocdashboard] - brought to life by the amazing data magician Lisa Hehnke - for a more detailed analysis of our user base), we have begun to more and more consider foregrounding aspects of inclusivity and equity.

Right from Day One, we have always negotiated the trade-offs between easy availability of services, usability and pragmatic access, and counter-questions towards real openness that includes being able to control what happens to one's personal and project data, particularly from proponents arguing along the lines of the Free, Libre and Open Source (FLOSS) community. Also, did we mention that with no available project budget to speak of, we have been, and still are, dependent on freely available services? Right...

Now, as Richard Stallmann has it, *free* in the sense of a variety of open movements should entail not only the gratis - or *free beer* - aspect of basic subscription to commercial services, but also the *libre* element  - as in *free speech*. This included long, and recurring debates about if we e.g. want to use Google Docs or rather should explore other, more privacy-phile ventures, since Google (or *G-Corp*, as many of us simply address the internet giant in
reference to the fictional E-Corp of *Mr. Robot* fame - although Open Publishing sure has an E-Corp of its own ;) ) definitely feeds on our data (see e.g. [Forbes][forbes], or [this account of more disturbing consequences][npr], or [this2][creativegood] of what Google does with its search algorithms).

![Someone looking at the E-Corp statue in Mr. Robot TV show. Animation.](https://gitlab.com/flavoursofopenscience/grav/raw/master/experimental/e-corp.gif)

We have long tried to run the OpenScienceMOOC's development backbone via GitHub.com, a free service that itself used to be a harbinger of open source projects, but had to face tremendous loss in support by the open source community over the last few years, particularly since its having been bought by Microsoft. Things have gotten worse when, over the last few months, there's also been a massive increase in selective refusals of service and block-outs of researchers by GitHub.com, which faced being used as a tool by the United States' government enforcing trade
policy and sanctions against countries such as Cuba and Iran (see e.g. [LinuxInsider][linuxinsider], [ZDNet][zdnet]).

Adding to that, we began to use Slack as a group communication platform back in 2017, but since the free service only allows access to a limited backlog of messages in their archive (see [Slack Free Plan limitations][slack]), and Slack as a US-based business is bound by the same legal constraints that are enforced via GitHub.com, it quickly became clear that we would need an alternative solution that allows us to reclaim control of our data, because the limited archive means that we continue to lose older messages that will become irretrievable.

Now, since such manifestations of corporate behaviour as have become obvious via the above-described incidents could directly stifle the OSMOOC's ability to include researchers from all regions to the MOOC's development, let alone the moral questions and consequences raised by such Orwellian selective control of one government over science, research and scholarship without due legal process, we are now looking for alternative options.

What this amounts to is overt control over our data that even transcends what Budroni et al. have recently described as a world where 
> "*80 % of [our] data are then collected and stored in the U.S., mainly on servers in the Silicon Valley or near Seattle [, ... enabling] the analysts working outside Europe [...] to track and
understand habits, trends, learnings of communities [...] better than the European governing bodies of the Member States or the European Commission.*" ([2019][abitechdoi]).

What we are looking at is even worse, because the incidents described point to issues that are not only about tracking and analysis, but also about data security breaches, non-conformity with even basic privacy rights, and an enforcement of policies that none of us would never have considered possible.

All of this, along with the personal experiences of many members of our steering committee, now has us on the lookout for options that would allow us to take project management and communications into our own hands. In order to stay true
to the open source FLOSS perspective, we now want to set up a stack of web tools that are known to embody this spirit, and have those self-hosted somewhere.

**The question that we have been asking ourselves, then, is: Are we really alone with this need for openly-available infrastructures for training, education and scholarly communication that more closly stick to open principles?**

Because in the end, it does not really matter if you rely on GitHub.com or GitLab.com, use [Google Docs][hackernoon], [Slack][slackzdnet], [Trello][theintercept], [Twitter][twitterleak], [Skype][skypesurveillance], [Office365][office365ban], [Facebook][techtarget], or any other corporate service. Since all of these services are run on US-based servers, they are all prone to be used for purposes that run counter to one's personal privacy or institutional legal frameworks (see e.g. the sudden and massive loss of content when [Microsoft decided to close down its DRM-locked ebook store][msebookalypse], or the [loss of massive troves of user account data][wikipedia] due to lax security measures), let alone that they become utilized as tools to enforce questionable national policies...

Overall, more elegant and sensible solutions to these dilemmata such as non-profit, community-led approaches have been outlined by many scholars since the dawn of the internet - let me just highlight one example from the not-so-recent scholarly history of thirteen years ago, on "[A Cooperative Publishing Model for Sustainable Scholarship][schroedersiegel]". Sounds familiar? That is because at large - and with notable exemptions of great initiatives in publishing including ScholarLed, osf.io and the Open Library of Humanities - it seems as if we're running around in circles, leaving particularly the field of training and education to corporate interests.

Drawing from the experiences made with the OpenScienceMOOC, we think that for reasons of sustainability and general acceptance among the existing camps, factions and fields of open, let alone national and field-specific delineations, it would be best if a supranational entitiy such as the EU would be hosting such tools. What we would need is making a tool stack openly available that should comprise training and scholarly communication tools and platforms such as:

- [GitLab Community Edition][gitlab-ce] as a self-hosted code sharing and project management platform with issue tracker, wiki, Kanban board, and more;
- [Mattermost][mattermost] as an established open source alternative to Slack for scholarly communication in self-governed fora, and with the benefit of full control of one's data;
- [Nextcloud][nextcloud] for file sharing purposes to replace commercial services such as Google Drive or Dropbox;
- [Collabora Online][collabora-online] or [OnlyOffice][onlyoffice] integrated to NextCloud as an alternative to the omnipresent Google Docs Suite that is still often perceived as the most versatile solution for easy collaboration.

...because in the end, what we as scholars should have learned by now is that an altruistic provision of services is **not** in the best interest of corporate strategy. This is why we think it is crucial to highlight the need of further
public-sector investment in the tools necessary to facilitate training, teaching and learning of and with Open Scholarship, so as to ensure that public access to knowledge in all its forms can become an equitable process.

*Postscriptum: The author is writing in a personal capacity. None of the above should be taken as the view or position of the author's employers or other organisations.*

## Further reading:

- Bilder, G., Lin, J. and Neylon, C. (2015) Principles for Open Scholarly Infrastructure-v1,
  retrieved 2019/08/15, doi: [10.6084/m9.figshare.1314859][figsharedoi1]
- Bosman, J. & Kramer, B. (2017) The European Open Science Cloud as a commons? retrieved 2019/08/15, doi: [10.6084/m9.figshare.5537899.v1][figsharedoi2]
- Budroni, P., Claude-Burgelman, J. & Schouppe, M. (2019). Architectures of
  Knowledge: The European Open Science Cloud. ABI Technik, 39(2), pp. 130-141.
  retrieved 2019/08/15, doi: [10.1515/abitech-2019-2006][abitechdoi]
- McGonagle‐O’Connell, A. and Ratan, K. (2019), Can we transform scholarly
  communication with open source and community‐owned infrastructure?. Learned
  Publishing, 32: 75-78. retrieved 2019/08/15, doi: [10.1002/leap.1215][leapdoi]
- Pooley, J. (2018) Scholarly communications shouldn’t just be open, but non-profit too. [LSE Impact blog][lseiblink], retrieved 2019/08/15.
- Schroeder, R. and Siegel, G. E. (2006) A Cooperative Publishing Model for Sustainable Scholarship. Library Faculty Publications and Presentations. 66. doi: [10.1353/scp.2006.0006][schroedersiegeldoi], [preprint][schroedersiegel]

[collabora-online]: https://www.collaboraoffice.com/collabora-online/
[gitlab-ce]: https://gitlab.com/gitlab-org/gitlab-ce/
[mattermost]: https://www.mattermost.org/
[nextcloud]: https://nextcloud.com/
[onlyoffice]: https://www.onlyoffice.com/
[opensciencemooc]: https://opensciencemooc.eu/
[osmoocdashboard]: https://www.dataplanes.org/osmooc-dashboard/
[forbes]: https://www.forbes.com/sites/kateoflahertyuk/2019/08/01/warning-issued-over-google-chrome-ad-blocking-plans/#413dadce219a
[wikipedia]: https://en.wikipedia.org/wiki/List_of_data_breaches
[npr]: https://www.npr.org/2019/02/22/696949013/advertisers-abandon-youtube-over-concerns-that-pedophiles-lurk-in-comments-secti?t=1566209097077
[creativegood]: https://creativegood.com/blog/19/google-profits-from-pedophiles.html
[linuxinsider]: https://www.linuxinsider.com/story/86154.html
[zdnet]: https://www.zdnet.com/article/github-starts-blocking-developers-in-countries-facing-us-trade-sanctions/
[slack]: https://get.slack.help/hc/en-us/articles/115002422943-Message-file-storage-and-app-limits-on-the-Free-plan
[twitterleak]: https://techcrunch.com/2019/08/07/twitter-fesses-up-to-more-adtech-leaks/
[skypesurveillance]: https://www.vice.com/en_us/article/xweqbq/microsoft-contractors-listen-to-skype-calls
[office365ban]: https://arstechnica.com/information-technology/2019/07/germany-threatens-to-break-up-with-microsoft-office-again/
[msebookalypse]: https://www.wired.com/story/microsoft-ebook-apocalypse-drm/
[figsharedoi1]: https://doi.org/10.6084/m9.figshare.1314859
[abitechdoi]: https://doi.org/10.1515/abitech-2019-2006
[leapdoi]: https://doi.org/10.1002/leap.1215
[figsharedoi2]: https://doi.org/10.6084/m9.figshare.5537899.v1
[lseiblink]: https://blogs.lse.ac.uk/impactofsocialsciences/2017/08/15/scholarly-communications-shouldnt-just-be-open-but-non-profit-too/
[slackzdnet]: https://www.zdnet.com/article/slack-resets-passwords-for-1-of-its-users-because-of-2015-hack/
[hackernoon]: https://hackernoon.com/data-privacy-concerns-with-google-b946f2b7afea
[theintercept]: https://theintercept.com/2018/08/16/trello-board-uk-canada/
[techtarget]: https://searchsecurity.techtarget.com/news/252462588/A-recent-history-of-Facebook-security-and-privacy-issues
[schroedersiegel]: https://pdxscholar.library.pdx.edu/ulib_fac/66/
[schroedersiegeldoi]: https://doi.org/10.1353/scp.2006.0006
[osmoocpeople]: https://opensciencemooc.eu/people/
